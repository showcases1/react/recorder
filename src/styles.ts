import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }
    
    body {
        font-size: 16px;
        font-family: 'SF UI Text', sans-serif;
        font-weight: 500;
        display:flex;
    }
    
    button, input, select {
        outline: none;
        border: none;
        background: none;
        font-size: 16px;
        font-family: 'SF UI Text', sans-serif;
        font-weight: 500;
    }
    
    a {
        color: inherit;
        text-decoration: none;
    }

    #Recorder-plugin {
        width: 100%;
        height: 700px;

        background-color: white;

        overflow: auto;
    }
`;
