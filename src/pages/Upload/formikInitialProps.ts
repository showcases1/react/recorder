import { validateUploadForm } from 'pages/Upload/validation';

import { languagesCodes } from 'constants/languages';

const userLanguage = languagesCodes[navigator.language.slice(0, 2)];

export const uploadInitialProps = {
    validateOnChange: true,
    initialErrors: {
        title: ' ',
        mainProduct: ' ',
        secondaryProducts: ' ',
        language: ''
    },
    initialTouched: {
        title: true,
        mainProduct: true,
        secondaryProducts: true,
        language: true
    },
    initialValues: {
        title: '',
        mainProduct: '',
        secondaryProducts: [],
        language: userLanguage || 'English'
    },
    validateOnBlur: false,
    validate: validateUploadForm,
    onSubmit: () => {}
};
