import { FormikErrors, FormikValues } from 'formik';

interface FormErrors {
    title?: string;
    mainProduct?: string;
    secondaryProducts?: string[];
}

//
/*** Title ***/
//
export const validateTitle = async (title: string) => {
    if (!title) {
        return ' ';
    }
    if (title.length < 5) {
        return 'Oops, title is too short 😢';
    }
    if (title.length > 256) {
        return 'Oops, title is too long 😢';
    }

    return '';
};

//
/*** Chosen Product ***/
//
export const validateMainProduct = (value: string) => {
    if (!value) {
        return ' ';
    }

    return '';
};

//
/*** Main Form ***/
//
export const validateUploadForm = async ({ title, mainProduct }: FormikValues) => {
    const errors: FormikErrors<FormErrors> = {};

    errors.title = await validateTitle(title);
    errors.mainProduct = validateMainProduct(mainProduct);

    return errors;
};
