import styled, { css } from 'styled-components';

export const VideoInfoWrapper = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 21px;
`;

interface MainWrapperProps {
    isFinalStep: boolean;
}

export const MainWrapper = styled.section<MainWrapperProps>`
    ${({ isFinalStep }) => css`
        min-height: ${isFinalStep ? '100%' : 'calc(100% - 60px)'};
        padding: ${isFinalStep ? '0' : '28px 20px'};
        display: flex;
        flex-direction: column;

        ${isFinalStep &&
        css`
            ${VideoInfoWrapper} {
                display: none;
            }
        `}
    `}
`;

interface VideoPosterProps {
    poster: string;
}

export const VideoPoster = styled.div<VideoPosterProps>`
    ${props => css`
        width: 70px;
        height: 119px;
        margin-right: 17px;
        border-radius: 10px;
        background-position: center;
        background-size: cover;
        background-image: url(${props.poster});
    `}
`;

export const VideoName = styled.b`
    font-size: 15px;
    font-weight: 800;
`;

export const VideoDuration = styled.div`
    font-size: 15px;
`;

export const StepTitle = styled.h2`
    font-size: 18px;
`;

interface StepWrapperProps {
    paddingTop: number;
}

export const StepWrapper = styled.div<StepWrapperProps>`
    ${props => css`
        min-height: 192px;
        padding-top: ${props.paddingTop}px;
        margin-bottom: 28px;
    `}
`;

export const NextButtonWrapper = styled.div`
    display: flex;
    justify-content: flex-end;
`;
