import React, { FC } from 'react';
import { Formik } from 'formik';
import { useStore } from 'effector-react';

import { useSteps } from 'hooks';
import { state } from 'effects/video';
import { uploadInitialProps } from './formikInitialProps';

import { MainWrapper, VideoInfoWrapper, VideoPoster, VideoName, VideoDuration } from './styles';
import { TopBar } from 'components/common/TopBar';
import { TitleStep, ProductsStep, LanguageStep, FinalStep } from 'pages/Upload/Steps';

import { UploadStepName, UploadStepsObject, UploadStepProps } from 'types/upload';

const stepsObj: UploadStepsObject = {
    TitleStep,
    ProductsStep,
    LanguageStep,
    FinalStep
};

export const UploadPage: FC = () => {
    const [CurrentComponent, setCurrentStep, goToPrevStep, currentStep] = useSteps<UploadStepName, UploadStepProps>(
        stepsObj
    );
    const duration = useStore(state.recorderVideoDuration);
    const recorderVideoScreen = useStore(state.recorderVideoScreen);
    const recorderVideoDate = useStore(state.recorderVideoDate);

    return (
        <Formik {...uploadInitialProps}>
            <>
                {currentStep !== 'FinalStep' && <TopBar onButtonClick={goToPrevStep} />}
                <MainWrapper isFinalStep={currentStep === 'FinalStep'}>
                    <VideoInfoWrapper>
                        <VideoPoster poster={recorderVideoScreen} />
                        <div>
                            <VideoName>{recorderVideoDate}.mov</VideoName>
                            <VideoDuration>Duration {Number(duration).toFixed(2)} seconds</VideoDuration>
                        </div>
                    </VideoInfoWrapper>
                    <CurrentComponent goToPrevStep={goToPrevStep} setCurrentStep={setCurrentStep} />
                </MainWrapper>
            </>
        </Formik>
    );
};
