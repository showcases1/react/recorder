import React, { FC, FormEvent } from 'react';
import { Form } from 'formik';

import { languagesArray } from 'constants/languages';

import { NextButtonWrapper, StepTitle, StepWrapper } from 'pages/Upload/styles';
import { Select } from 'components/ReachUI/Select';
import { NavigationButton } from 'components/common/NavigationButton';

import { UploadStepProps } from 'types/upload';

export const LanguageStep: FC<UploadStepProps> = ({ setCurrentStep }) => {
    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        setCurrentStep('FinalStep');
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Spoken Language</StepTitle>
            <StepWrapper paddingTop={41}>
                <Select name="language" values={languagesArray} />
            </StepWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit />
            </NextButtonWrapper>
        </Form>
    );
};
