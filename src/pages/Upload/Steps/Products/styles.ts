import styled, { css } from 'styled-components';

import checkMark from 'assets/icons/check-mark.svg';

interface AddedProductProps {
    secondary?: boolean;
}

export const AddedProduct = styled.div<AddedProductProps>`
    ${props => css`
        font-weight: bold;
        padding: 2px 0 2px 40px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        position: relative;
        word-break: break-all;

        ${props.secondary &&
        css`
            margin-left: 40px;
        `}

        &::before {
            content: url(${checkMark});
            position: absolute;
            width: 20px;
            height: 20px;
            left: 0;
            top: 50%;
            transform: translateY(-50%);
        }
    `}
`;

export const AddedProductsWrapper = styled.div`
    padding-bottom: 30px;

    ${AddedProduct}:not(:last-child) {
        margin-bottom: 24px;
    }
`;

export const Dots = styled.div`
    padding: 0 10px;
    margin-right: -10px;
    margin-left: 20px;
`;

export const Dot = styled.div`
    width: 4px;
    height: 4px;
    border-radius: 50%;
    background: #000;
    &:not(:last-child) {
        margin-bottom: 4px;
    }
`;

export const AddButtonWrapper = styled.div`
    display: flex;
    justify-content: flex-end;
`;
