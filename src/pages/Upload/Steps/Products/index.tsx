import { Button } from 'components/common/Button';
import { NavigationButton } from 'components/common/NavigationButton';
import { ProductCombobox } from 'components/ReachUI/ProductCombobox';
import { useField } from 'formik';
import { AddButtonWrapper, AddedProduct, AddedProductsWrapper, Dot, Dots } from 'pages/Upload/Steps/Products/styles';
import { NextButtonWrapper, StepTitle, StepWrapper } from 'pages/Upload/styles';
import React, { FC, useState } from 'react';
import { UploadStepProps } from 'types/upload';

export const ProductsStep: FC<UploadStepProps> = ({ setCurrentStep }) => {
    const [isAddingSecondaryProduct, setIsAddingSecondaryProduct] = useState(false);
    const [, mainProductMeta, mainProductHelpers] = useField('mainProduct');
    const [, secondaryProductsMeta, secondaryProductsHelpers] = useField('secondaryProducts');
    const hasError = !!mainProductMeta.error;

    const mainProductName = mainProductMeta.value && mainProductMeta.value.name;
    const secondaryProducts = secondaryProductsMeta.value;

    const handleMainProductDelete = () => {
        mainProductHelpers.setValue('');
    };

    const makeSecondaryProductDeleteHandler = (name: string) => () => {
        const newArray = secondaryProducts.filter((product: Recorder.ProductResponse) => product.name !== name);

        secondaryProductsHelpers.setValue(newArray);
    };

    const handleAddProductClick = () => {
        setIsAddingSecondaryProduct(true);
    };

    const goToNextStep = () => {
        if (hasError) {
            return;
        }

        setCurrentStep('LanguageStep');
    };

    return (
        <>
            <StepTitle>Products in Video</StepTitle>
            <StepWrapper paddingTop={24}>
                {!!mainProductName && (
                    <AddedProductsWrapper>
                        <AddedProduct>
                            {mainProductName}
                            <Dots onClick={handleMainProductDelete}>
                                <Dot />
                                <Dot />
                                <Dot />
                            </Dots>
                        </AddedProduct>
                        {secondaryProducts.map((product: Recorder.ProductResponse) => (
                            <AddedProduct key={product.name} secondary>
                                {product.name}
                                <Dots onClick={makeSecondaryProductDeleteHandler(product.name || '')}>
                                    <Dot />
                                    <Dot />
                                    <Dot />
                                </Dots>
                            </AddedProduct>
                        ))}
                    </AddedProductsWrapper>
                )}
                {!mainProductName || isAddingSecondaryProduct ? (
                    <ProductCombobox
                        secondary={!!mainProductName}
                        setIsAddingSecondaryProduct={setIsAddingSecondaryProduct}
                    />
                ) : (
                    <AddButtonWrapper>
                        <Button view="inline" onClick={handleAddProductClick}>
                            + add product
                        </Button>
                    </AddButtonWrapper>
                )}
            </StepWrapper>
            <NextButtonWrapper>
                <NavigationButton circle disabled={hasError} onClick={goToNextStep} />
            </NextButtonWrapper>
        </>
    );
};
