import { useStore } from 'effector-react';
import { effects as productEffects } from 'effects/product';
import { effects as stepsEffects, Steps } from 'effects/steps';
import { effects as videoEffects, state as videoState } from 'effects/video';
import { useField } from 'formik';
import {
    ButtonsWrapper,
    CancelButton,
    ContentWrapper,
    DarkenBg,
    MainWrapper,
    Qualities,
    Stake,
    StyledButton,
    Text,
    Title
} from 'pages/Upload/Steps/Final/styles';
import React, { FC, useState } from 'react';
import { UploadStepProps } from 'types/upload';

type LoadingButton = '' | 'Submit' | 'Upload';

export const FinalStep: FC<UploadStepProps> = ({ goToPrevStep }) => {
    const [loadingButton, setLoadingButton] = useState<LoadingButton>('');
    const recorderVideoScreen = useStore(videoState.recorderVideoScreen);
    const [, titleMeta] = useField('title');
    const [, mainProductMeta] = useField('mainProduct');
    const [, secondaryProductsMeta] = useField('secondaryProducts');
    const [, languageMeta] = useField('language');

    const createNewVideoRequestObject = async () => {
        const requestObj: Recorder.CreateNewVideoRequest = {
            title: titleMeta.value,
            subtitle: '',
            primaryProductId: mainProductMeta.value.id,
            audioLanguages: [languageMeta.value]
        };

        if (!requestObj.primaryProductId) {
            const response = await productEffects.createProduct(mainProductMeta.value.name);

            if (!response.id) {
                return;
            }

            requestObj.primaryProductId = response.id;
        }

        const secondaryProductIdsPromises = secondaryProductsMeta.value.map(
            async (product: Recorder.ProductResponse) => {
                if (!product.id) {
                    const response = await productEffects.createProduct(product.name as string);

                    return response.id;
                } else {
                    return product.id;
                }
            }
        );
        requestObj.secondaryProductIds = await Promise.all(secondaryProductIdsPromises);

        return requestObj;
    };

    const uploadVideo = async () => {
        const requestObj = await createNewVideoRequestObject();
        const videoBlob = videoState.recorderVideo.getState();

        if (!requestObj || !videoBlob) {
            throw new Error();
        }

        const newVideoResponse = await videoEffects.createNewVideoRequest(requestObj);

        const uploadResponse = await videoEffects.uploadVideo({
            videoId: newVideoResponse.videoUploadId || '',
            videoStream: videoBlob
        });

        if (uploadResponse.isSuccess) {
            return newVideoResponse;
        } else {
            throw new Error();
        }
    };

    const submitVideo = async () => {
        const uploadedVideo = await uploadVideo();

        if (uploadedVideo) {
            const validationResponse = await videoEffects.validateContent({
                contentId: uploadedVideo.videoId || '',
                stakeAmount: 1,
                requestStakingPromotion: true,
                holdUntilApproved: true
            });

            if (validationResponse) {
                stepsEffects.setStep(Steps.ThankYou);
            }
        }
    };

    const handleUpload = async () => {
        setLoadingButton('Upload');

        try {
            const uploadedVideo = await uploadVideo();

            if (uploadedVideo) {
                stepsEffects.setStep(Steps.ThankYou);
            }
        } catch {
            setLoadingButton('');
        }
    };

    const handleSubmit = async () => {
        setLoadingButton('Submit');

        try {
            await submitVideo();
        } catch {
            setLoadingButton('');
        }
    };

    return (
        <MainWrapper poster={recorderVideoScreen}>
            <DarkenBg />
            <ContentWrapper>
                <Title>Video Upload</Title>
                <Text>
                    If you would like to earn WOM tokens for having people watch and engage with your video, we need to
                    have it rated for
                    <Qualities>
                        Honesty <br />
                        Creativity <br />
                        Positivity
                    </Qualities>
                </Text>
                <Text>
                    This review requires a <br />
                    stake of
                </Text>
                <Stake>1.0</Stake>
                <Text>This one is on us</Text>
            </ContentWrapper>
            <ButtonsWrapper>
                <StyledButton isLoading={loadingButton === 'Submit'} view="green" onClick={handleSubmit}>
                    Submit now
                </StyledButton>
                <StyledButton isLoading={loadingButton === 'Upload'} view="white" onClick={handleUpload}>
                    Upload only
                </StyledButton>
                <CancelButton view="inline" onClick={goToPrevStep}>
                    Cancel
                </CancelButton>
            </ButtonsWrapper>
        </MainWrapper>
    );
};
