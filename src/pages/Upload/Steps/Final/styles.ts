import styled, { css } from 'styled-components';

import { Button } from 'components/common/Button';

interface MainWrapperProps {
    poster?: string;
}

export const MainWrapper = styled.section<MainWrapperProps>`
    ${({ poster }) => css`
        padding: 57px 0;
        text-align: center;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        background: #000 url(${poster}) no-repeat center;
        background-size: cover;
        position: relative;
        flex-grow: 1;

        * {
            position: relative;
        }
    `}
`;

export const ContentWrapper = styled.div`
    max-width: 250px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const ButtonsWrapper = styled(ContentWrapper)`
    margin-top: 60px;
`;

export const DarkenBg = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.8);
`;

export const Title = styled.h1`
    color: #fff;
    font-weight: 800;
    font-size: 24px;
    margin-bottom: 30px;
`;

export const Text = styled.div`
    color: #fff;
    font-size: 14px;
    line-height: 26px;
`;

export const Qualities = styled.p`
    font-size: 18px;
    margin-bottom: 45px;
`;

export const Stake = styled.div`
    width: 148px;
    height: 41px;
    margin: 11px 0 6px;
    flex: none;
    background: #fff;
    border-radius: 10px;
    line-height: 41px;
    font-weight: 800;
`;

export const StyledButton = styled(Button)`
    width: 231px;
    margin-bottom: 15px;
`;

export const CancelButton = styled(Button)`
    margin-top: 5px;
    color: #fff;
`;
