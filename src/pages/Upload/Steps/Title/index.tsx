import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { NextButtonWrapper, StepTitle, StepWrapper } from 'pages/Upload/styles';
import { TitleField } from 'components/Formik/TitleField';
import { NavigationButton } from 'components/common/NavigationButton';

import { UploadStepProps } from 'types/upload';

export const TitleStep: FC<UploadStepProps> = ({ setCurrentStep }) => {
    const [, meta] = useField('title');
    const hasError = !!meta.error;

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        setCurrentStep('ProductsStep');
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Title</StepTitle>
            <StepWrapper paddingTop={52}>
                <TitleField />
            </StepWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NextButtonWrapper>
        </Form>
    );
};
