import styled from 'styled-components';

import { Button } from 'components/common/Button';

import bgImage from 'assets/entry-bg.png';

export const PageWrapper = styled.section`
    background: #000 url(${bgImage}) center 0;
    background-size: cover;
    min-height: 100%;
    padding: 15% 20px 8%;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export const Logo = styled.img`
    margin-bottom: 20px;
    width: 200px;
`;

export const Title = styled.h1`
    font-family: 'SF UI Display', sans-serif;
    color: #fff;
    font-size: 21px;
    line-height: 28px;
    margin: 70px 0 110px;
`;

export const GoButton = styled(Button)`
    width: 240px;
`;
