import React from 'react';

import { effects, Steps } from 'effects/steps';

import { GoButton, Logo, PageWrapper, Title } from 'pages/Entry/styles';

import logoImage from 'assets/logo.png';

export const EntryPage = () => {
    const goToNextStep = () => {
        effects.setStep(Steps.VideoRecord);
    };

    return (
        <PageWrapper>
            <Logo src={logoImage} />
            <Title>
                Create awesome <br />
                recommendations and earn <br />
                Word-of-Mouth tokens
            </Title>
            <GoButton view="ghost" onClick={goToNextStep}>
                Let's go
            </GoButton>
        </PageWrapper>
    );
};
