import React, { useEffect } from 'react';
import { Formik } from 'formik';

import { useSteps } from 'hooks';
import { loginInitialProps } from 'pages/Sign/formikInitialProps';
import { effects } from 'effects/sign';

import { EmailOrPhoneStep, PasswordStep } from './Steps';
import { TopBar } from 'components/common/TopBar';
import { MainWrapper } from 'pages/Sign/styles';

import { LoginStepName, LoginStepsObject, SignStepProps } from 'types/sign';

const stepsObj: LoginStepsObject = {
    EmailOrPhoneStep,
    PasswordStep
};

export const LoginPage = () => {
    const [CurrentComponent, setCurrentStep, goToPrevStep] = useSteps<LoginStepName, SignStepProps>(stepsObj);

    // Unmount cleanup
    useEffect(
        () => () => {
            effects.changeChosenSignInput('');
        },
        []
    );

    return (
        <Formik {...loginInitialProps}>
            <div>
                <TopBar title="Login" onButtonClick={goToPrevStep} />
                <MainWrapper>
                    <CurrentComponent setCurrentStep={setCurrentStep} />
                </MainWrapper>
            </div>
        </Formik>
    );
};
