import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';
import { useStore } from 'effector-react';

import { effects as stepsEffects, Steps } from 'effects/steps';
import { effects, state as signState } from 'effects/sign';

import { Footnote, FootnoteLink, InputsWrapper, NoMarginNextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { PhoneField } from 'components/Formik/PhoneField';
import { EmailField } from 'components/Formik/EmailField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const EmailOrPhoneStep: FC<SignStepProps> = ({ setCurrentStep }) => {
    const chosenInput = useStore(signState.chosenSignInput);
    const [, meta] = useField(chosenInput || 'useField');
    const hasError = !chosenInput || !!meta.error;

    const choosePhone = () => {
        effects.changeChosenSignInput('phone');
    };
    const chooseEmail = () => {
        effects.changeChosenSignInput('email');
    };

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        setCurrentStep('PasswordStep');
    };

    const goToRegistrationPage = () => {
        stepsEffects.setStep(Steps.Registration);
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Enter your email or phone number</StepTitle>
            <InputsWrapper>
                {(!chosenInput || chosenInput === 'phone') && (
                    <PhoneField withLeftHelper={!!chosenInput} onFocus={choosePhone} />
                )}
                {(!chosenInput || chosenInput === 'email') && (
                    <EmailField login withLeftHelper={!!chosenInput} onFocus={chooseEmail} />
                )}
            </InputsWrapper>
            <Footnote>
                Not a member? <FootnoteLink onClick={goToRegistrationPage}>Register here</FootnoteLink>
            </Footnote>
            <NoMarginNextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NoMarginNextButtonWrapper>
        </Form>
    );
};
