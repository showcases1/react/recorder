import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { effects as signEffects } from 'effects/sign';
import { effects as userEffects } from 'effects/user';
import { effects as stepsEffects, Steps } from 'effects/steps';
import { useEffectsPendingStatus } from 'hooks';

import { InputsWrapper, NextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { PasswordField } from 'components/Formik/PasswordField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const PasswordStep: FC<SignStepProps> = () => {
    const isLoading = useEffectsPendingStatus(signEffects.authenticateUser);
    const [, passwordMeta, helpers] = useField('password');
    const [, emailMeta] = useField('email');
    const [, countryCodeMeta] = useField('countryCode');
    const [, phoneMeta] = useField('phone');
    const hasError = !!passwordMeta.error;

    const handleFormSubmit = async (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        const response = await signEffects.authenticateUser({
            usernameOrEmail: emailMeta.value,
            mobileNumber: countryCodeMeta.value + phoneMeta.value,
            password: passwordMeta.value
        });

        if (response && response.token && response.user) {
            userEffects.setToken(response.token);
            userEffects.setUser(response.user);

            if (response.user.isAccountVerified && response.user.isEmailValidated) {
                stepsEffects.setStep(Steps.Upload);
            } else {
                stepsEffects.setStep(Steps.Confirmation);
            }
        } else if (response) {
            helpers.setError('Oops, Login failed');
        }
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Enter your password</StepTitle>
            <InputsWrapper>
                <PasswordField login isLoading={isLoading} />
            </InputsWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NextButtonWrapper>
        </Form>
    );
};
