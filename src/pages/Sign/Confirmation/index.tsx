import React, { FC } from 'react';
import { useStore } from 'effector-react';

import { state as userState } from 'effects/user';
import { effects as signEffects } from 'effects/sign';
import { effects as stepsEffects, Steps } from 'effects/steps';
import { effects as notificationsEffects } from 'effects/notifications';

import { MainWrapper, Text, Email, ButtonsWrapper } from 'pages/Sign/Confirmation/styles';
import { Button } from 'components/common/Button';

export const ConfirmationPage: FC = () => {
    const isAuthCheckLoading = useStore(signEffects.getCurrentAuthorization.pending);
    const isWOMCreationLoading = useStore(signEffects.createWOMWallet.pending);
    const isConfirmationLoading = isAuthCheckLoading || isWOMCreationLoading;
    // const isResendingLoading = false;
    const { userId, email } = userState.user.getState();

    // const resend = () => {
    // };

    const reRegister = () => {
        stepsEffects.setStep(Steps.Registration);
    };

    const confirm = async () => {
        const authResponse = await signEffects.getCurrentAuthorization(userId);

        if (authResponse && authResponse.isAccountVerified && authResponse.isEmailValidated) {
            const walletResponse = await signEffects.createWOMWallet(true); // at least 1 argument needed

            if (walletResponse) {
                stepsEffects.setStep(Steps.Upload);
            }
        } else if (authResponse) {
            notificationsEffects.setNotification({
                message: 'Sorry, not confirmed yet',
                description: ''
            });
        }
    };

    return (
        <MainWrapper>
            <div>
                <Text>We have sent a confirmation email to</Text>
                <Email>{email}</Email>
                <Text>Please confirm your email before continuing</Text>
            </div>
            <ButtonsWrapper>
                {/*<Button isLoading={isResendingLoading} view="green" onClick={resend}>*/}
                {/*    RESEND*/}
                {/*</Button>*/}
                <Button view="grey" onClick={reRegister}>
                    RE-REGISTER
                </Button>
                <Button isLoading={isConfirmationLoading} view="dark" onClick={confirm}>
                    I ALREADY CONFIRMED
                </Button>
            </ButtonsWrapper>
        </MainWrapper>
    );
};
