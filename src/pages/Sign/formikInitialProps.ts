import { validateLoginForm, validatePasswordForgottenForm, validateRegistrationForm } from 'pages/Sign/validation';
import { getUserCountryId } from 'utils/getUserCountryId';

export const registrationInitialProps = {
    validateOnChange: true,
    initialErrors: {
        username: ' ',
        countryCode: '',
        phone: ' ',
        email: ' ',
        smsCode: ' ',
        password: ' '
    },
    initialTouched: {
        username: true,
        countryCode: true,
        phone: true,
        email: true,
        smsCode: true,
        password: true
    },
    initialValues: {
        username: '',
        countryCode: '',
        countryId: getUserCountryId() || 'us',
        phone: '',
        email: '',
        smsCode: '',
        password: ''
    },
    validateOnBlur: false,
    validate: validateRegistrationForm,
    onSubmit: () => {}
};

export const loginInitialProps = {
    validateOnChange: true,
    initialErrors: {
        countryCode: ' ',
        phone: ' ',
        email: ' ',
        smsCode: ' ',
        password: ' '
    },
    initialTouched: {
        countryCode: true,
        phone: true,
        email: true,
        smsCode: true,
        password: true
    },
    initialValues: {
        countryCode: '',
        countryId: getUserCountryId() || 'us',
        phone: '',
        email: '',
        smsCode: '',
        password: ''
    },
    validateOnBlur: false,
    validate: validateLoginForm,
    onSubmit: () => {}
};

export const passwordForgottenInitialProps = {
    validateOnChange: true,
    initialErrors: {
        email: ' ',
        password: ' ',
        securityCode: ' '
    },
    initialTouched: {
        email: true,
        password: true,
        securityCode: true
    },
    initialValues: {
        email: '',
        password: '',
        securityCode: ''
    },
    validateOnBlur: false,
    validate: validatePasswordForgottenForm,
    onSubmit: () => {}
};
