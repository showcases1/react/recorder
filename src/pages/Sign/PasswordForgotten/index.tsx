import React from 'react';
import { Formik } from 'formik';

import { useSteps } from 'hooks';
import { passwordForgottenInitialProps } from 'pages/Sign/formikInitialProps';

import { EmailStep, PasswordAndCodeStep } from './Steps';
import { TopBar } from 'components/common/TopBar';
import { MainWrapper } from 'pages/Sign/styles';

import { PasswordForgottenStepName, PasswordForgottenStepsObject, SignStepProps } from 'types/sign';

const stepsObj: PasswordForgottenStepsObject = {
    EmailStep,
    PasswordAndCodeStep
};

export const PasswordForgottenPage = () => {
    const [CurrentComponent, setCurrentStep, goToPrevStep] = useSteps<PasswordForgottenStepName, SignStepProps>(
        stepsObj
    );

    return (
        <Formik {...passwordForgottenInitialProps}>
            <div>
                <TopBar onButtonClick={goToPrevStep} />
                <MainWrapper>
                    <CurrentComponent setCurrentStep={setCurrentStep} />
                </MainWrapper>
            </div>
        </Formik>
    );
};
