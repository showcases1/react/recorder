import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { effects as signEffects } from 'effects/sign';
import { effects as userEffects } from 'effects/user';
import { effects as stepsEffects, Steps } from 'effects/steps';
import { useEffectsPendingStatus } from 'hooks';

import { InputsWrapper, NextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { PasswordField } from 'components/Formik/PasswordField';
import { SecurityCodeField } from 'components/Formik/SecurityCodeField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const PasswordAndCodeStep: FC<SignStepProps> = () => {
    const isLoading = useEffectsPendingStatus(signEffects.setNewPassword);
    const [, emailMeta] = useField('email');
    const [, passwordMeta] = useField('password');
    const [, securityCodeMeta, securityCodeHelpers] = useField('securityCode');
    const hasError = !!passwordMeta.error || !!securityCodeMeta.error;

    const handleFormSubmit = async (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        const email = emailMeta.value;
        const password = passwordMeta.value;
        const confirmationToken = securityCodeMeta.value;

        const response = await signEffects.setNewPassword({ email, password, confirmationToken });

        if (response && response.token && response.user) {
            userEffects.setToken(response.token);
            userEffects.setUser(response.user);

            if (response.user.isAccountVerified && response.user.isEmailValidated) {
                stepsEffects.setStep(Steps.Upload);
            } else {
                stepsEffects.setStep(Steps.Confirmation);
            }
        } else if (response) {
            securityCodeHelpers.setError('Code is invalid');
        }
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Enter your new password and the security code we sent you</StepTitle>
            <InputsWrapper>
                <PasswordField forgotten />
                <SecurityCodeField />
            </InputsWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} isLoading={isLoading} />
            </NextButtonWrapper>
        </Form>
    );
};
