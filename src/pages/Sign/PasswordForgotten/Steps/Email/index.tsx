import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { effects } from 'effects/sign';
import { useEffectsPendingStatus } from 'hooks';

import { InputsWrapper, NextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { EmailField } from 'components/Formik/EmailField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const EmailStep: FC<SignStepProps> = ({ setCurrentStep }) => {
    const isLoading = useEffectsPendingStatus(effects.sendForgottenPasswordEmail);
    const [, meta, helpers] = useField('email');
    const hasError = !!meta.error;

    const handleFormSubmit = async (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        const response = await effects.sendForgottenPasswordEmail(meta.value);

        if (response && response.isSuccess) {
            setCurrentStep('PasswordAndCodeStep');
        } else if (response) {
            helpers.setError("Hey sorry, but we couldn't find your email address 😔");
        }
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Enter your email address</StepTitle>
            <InputsWrapper>
                <EmailField isLoading={isLoading} />
            </InputsWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NextButtonWrapper>
        </Form>
    );
};
