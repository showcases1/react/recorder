import { FormikErrors, FormikValues } from 'formik';

import { effects } from 'effects/sign';
import { isEmailDisposable } from 'utils/isEmailDisposable';

//TODO: prevent every field validation
interface FormErrors {
    username?: string;
    phone?: string;
    smsCode?: string;
    email?: string;
    password?: string;
    securityCode?: string;
}

type SingType = 'login' | 'registration' | 'forgotten';

//
/*** Username ***/
//
const validateUsername = async (username: string) => {
    if (!username || username === '@') {
        return ' ';
    }
    if (username[0] !== '@') {
        username = `@${username}`;
    }
    if (username.length < 7) {
        return 'Oops, your username is too short 😢';
    }

    // Formik catches 404 error
    const exists = await effects.checkUsernameExistence(username.slice(1));

    if (exists) {
        return 'Oops, your username is taken 😢';
    } else {
        return '';
    }
};

//
/*** Phone ***/
//
const validatePhone = async (phone: string, countryCode: string) => {
    if (!phone) {
        return ' ';
    }

    const result = await effects.analyzeMobileNumber(countryCode + phone);

    // 0 = Exists
    // 1 = NotFoundInSystem
    // -2 = PendingVerification
    // -1 = InvalidFormat
    switch (result) {
        case 0:
            return 'Oh WOW! We already know you! You should login instead, or change your phone...';
        case 1:
        case -2:
            return '';
        case -1:
            return 'Hey, sorry, but this does not looks like a valid phone number 😔';
        default:
            return ' ';
    }
};

//
/*** Email ***/
//
const validateEmail = async (email: string, signType?: SingType) => {
    if (!email) {
        return ' ';
    }

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(email)) {
        return 'Not quite a valid address….. 😁';
    }
    if (isEmailDisposable(email)) {
        return 'Hey, sorry, but we cannot use single use email addresses 😔';
    }
    if (signType === 'registration') {
        // 3 = exists
        const result = await effects.analyzeEmail(email);

        if (result === 3) {
            return 'Oh WOW! We already know you! You should login instead, or change your email addy...';
        }
    }

    return '';
};

//
/*** Sms Code ***/
//
const validateSmsCode = async (code: string, mobileNumber: string) => {
    if (code.length !== 6) {
        return 'Codes will be 6 digits long 😁';
    }

    const isCodeValid = await effects.checkSmsCode({ code, mobileNumber });

    if (isCodeValid) {
        return '';
    } else {
        return 'Invalid';
    }
};

//
/*** Password ***/
//
const validatePassword = async (password: string, signType?: SingType) => {
    if (signType === 'login') {
        return password.length ? '' : ' ';
    }

    const passwordRegex = /^(?=.*\d)(?=.*[A-Z]).{8,}$/;

    if (!passwordRegex.test(password)) {
        return 'Passwords should be eight or more characters and include a capital letter and a number 😁';
    } else {
        return '';
    }
};

//
/*** Security Code ***/
//
const validateSecurityCode = async (code: string) => (code.length ? '' : ' ');

//
/*** Main Forms ***/
//
export const validateRegistrationForm = async ({
    username,
    countryCode,
    phone,
    smsCode,
    email,
    password
}: FormikValues) => {
    const errors: FormikErrors<FormErrors> = {};

    errors.username = await validateUsername(username);
    errors.phone = await validatePhone(phone, countryCode);
    errors.email = await validateEmail(email, 'registration');
    errors.smsCode = await validateSmsCode(smsCode, countryCode + phone);
    errors.password = await validatePassword(password);

    return errors;
};

export const validateLoginForm = async ({ countryCode, phone, smsCode, email, password }: FormikValues) => {
    const errors: FormikErrors<FormErrors> = {};

    errors.phone = await validatePhone(phone, countryCode);
    errors.email = await validateEmail(email);
    errors.smsCode = await validateSmsCode(smsCode, countryCode + phone);
    errors.password = await validatePassword(password, 'login');

    return errors;
};

export const validatePasswordForgottenForm = async ({ email, securityCode, password }: FormikValues) => {
    const errors: FormikErrors<FormErrors> = {};

    errors.email = await validateEmail(email);
    errors.password = await validatePassword(password);
    errors.securityCode = await validateSecurityCode(securityCode);

    return errors;
};
