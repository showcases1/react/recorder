import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';
import { useStore } from 'effector-react';

import { effects, state } from 'effects/sign';
import { useEffectsPendingStatus } from 'hooks';

import { InputsWrapper, NextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { PhoneField } from 'components/Formik/PhoneField';
import { EmailField } from 'components/Formik/EmailField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const EmailOrPhoneStep: FC<SignStepProps> = ({ setCurrentStep }) => {
    const chosenInput = useStore(state.chosenSignInput);
    const isEmailLoading = useEffectsPendingStatus(effects.analyzeEmail);

    const [, chosenInputMeta] = useField(chosenInput || 'useField');
    const [, countryCodeMeta] = useField('countryCode');
    const hasError = !chosenInput || !!chosenInputMeta.error;

    const choosePhone = () => {
        effects.changeChosenSignInput('phone');
    };
    const chooseEmail = () => {
        effects.changeChosenSignInput('email');
    };

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        if (chosenInput === 'phone') {
            // If .then used instead of .finally,
            // 409 response throws error and setCurrentStep isn't fired
            effects.sendSmsCode(countryCodeMeta.value + chosenInputMeta.value).finally(() => {
                setCurrentStep('SmsCodeStep');
            });
        } else if (chosenInput === 'email') {
            setCurrentStep('PasswordStep');
        }
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>
                Connect your account to your <br /> Email or Phone
            </StepTitle>
            <InputsWrapper>
                {(!chosenInput || chosenInput === 'phone') && (
                    <PhoneField withLeftHelper={!!chosenInput} onFocus={choosePhone} />
                )}
                {(!chosenInput || chosenInput === 'email') && (
                    <EmailField isLoading={isEmailLoading} withLeftHelper={!!chosenInput} onFocus={chooseEmail} />
                )}
            </InputsWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NextButtonWrapper>
        </Form>
    );
};
