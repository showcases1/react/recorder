import { Button } from 'components/common/Button';
import { effects as signEffects, state as signState } from 'effects/sign';
import { effects as stepsEffects, Steps } from 'effects/steps';
import { effects as userEffects } from 'effects/user';
import { useField } from 'formik';
import { useEffectsPendingStatus } from 'hooks';
import React, { FC } from 'react';
import { ButtonWrapper, ContentWrapper, Link, MainWrapper, Subtitle, Text, Title } from './styles';

export const AgreementStep: FC = () => {
    const chosenSignInput = signState.chosenSignInput.getState();
    const isLoading = useEffectsPendingStatus(signEffects.createAccount);

    const [, usernameMeta] = useField('username');
    const [, countryCodeMeta] = useField('countryCode');
    const [, phoneMeta] = useField('phone');
    const [, emailMeta] = useField('email');
    const [, smsCodeMeta] = useField('smsCode');
    const [, passwordMeta] = useField('password');

    const handleDecline = () => {
        stepsEffects.setStep(Steps.Login);
    };

    const handleAccept = async () => {
        const accountResponse = await signEffects
            .createAccount({
                acceptLicense: true,
                username: usernameMeta.value.slice(1),
                email: chosenSignInput === 'email' ? emailMeta.value : undefined,
                mobileNumber: chosenSignInput === 'phone' ? countryCodeMeta.value + phoneMeta.value : undefined,
                smsVerificationCode: chosenSignInput === 'phone' ? smsCodeMeta.value : undefined,
                password: passwordMeta.value,
                language: navigator.language
            })
            .catch(err => err);

        if (accountResponse && accountResponse.token && accountResponse.user) {
            userEffects.setToken(accountResponse.token);
            userEffects.setUser(accountResponse.user);

            stepsEffects.setStep(Steps.Confirmation);
        }
    };

    return (
        <MainWrapper>
            <Title>To continue, you must accept our end user licensing agreement.</Title>
            <ContentWrapper>
                <Subtitle>
                    The agreement can be see{' '}
                    <Link href="https://www.Recorder.com/legalnotice" target="_blank">
                        HERE
                    </Link>
                </Subtitle>
                <Text>
                    It is important to note that we do not accept nudity or pornography involving people and we also do
                    not accept violence in any form involving humans or animals.
                    <br />
                    <br />
                    Violating content will be removed and the submitter of that material may be banned.
                </Text>
                <ButtonWrapper>
                    <Button view="grey" onClick={handleDecline}>
                        Decline
                    </Button>
                    <Button isLoading={isLoading} view="green" onClick={handleAccept}>
                        Accept
                    </Button>
                </ButtonWrapper>
            </ContentWrapper>
        </MainWrapper>
    );
};
