import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { InputsWrapper, NextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { PasswordField } from 'components/Formik/PasswordField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const PasswordStep: FC<SignStepProps> = ({ setCurrentStep }) => {
    const [, meta] = useField('password');
    const hasError = !!meta.error;

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        setCurrentStep('AgreementStep');
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Add a password</StepTitle>
            <InputsWrapper>
                <PasswordField />
            </InputsWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NextButtonWrapper>
        </Form>
    );
};
