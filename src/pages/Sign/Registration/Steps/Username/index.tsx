import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { effects, Steps } from 'effects/steps';

import { Footnote, FootnoteLink, InputsWrapper, NoMarginNextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { UsernameField } from 'components/Formik/UsernameField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const UsernameStep: FC<SignStepProps> = ({ setCurrentStep }) => {
    const [, meta] = useField('username');
    const hasError = !!meta.error;

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        setCurrentStep('EmailOrPhoneStep');
    };

    const goToLoginPage = () => {
        effects.setStep(Steps.Login);
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Create your account</StepTitle>
            <InputsWrapper>
                <UsernameField />
            </InputsWrapper>
            <Footnote>
                Already a member? <FootnoteLink onClick={goToLoginPage}>Login here</FootnoteLink>
            </Footnote>
            <NoMarginNextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NoMarginNextButtonWrapper>
        </Form>
    );
};
