import React, { FC, FormEvent } from 'react';
import { Form, useField } from 'formik';

import { InputsWrapper, NextButtonWrapper, StepTitle } from 'pages/Sign/styles';
import { SmsCodeField } from 'components/Formik/SmsCodeField';
import { NavigationButton } from 'components/common/NavigationButton';

import { SignStepProps } from 'types/sign';

export const SmsCodeStep: FC<SignStepProps> = ({ setCurrentStep }) => {
    const [, meta] = useField('smsCode');
    const hasError = !!meta.error;

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (hasError) {
            return;
        }

        setCurrentStep('PasswordStep');
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <StepTitle>Enter to security code we sent</StepTitle>
            <InputsWrapper>
                <SmsCodeField />
            </InputsWrapper>
            <NextButtonWrapper>
                <NavigationButton circle submit disabled={hasError} />
            </NextButtonWrapper>
        </Form>
    );
};
