import React, { useEffect } from 'react';
import { Formik } from 'formik';

import { useSteps } from 'hooks';
import { registrationInitialProps } from 'pages/Sign/formikInitialProps';
import { effects } from 'effects/sign';

import { UsernameStep, EmailOrPhoneStep, SmsCodeStep, PasswordStep, AgreementStep } from './Steps';
import { TopBar } from 'components/common/TopBar';
import { MainWrapper, RegistrationWrapper } from 'pages/Sign/styles';

import { RegistrationStepName, RegistrationStepsObject, SignStepProps } from 'types/sign';

const stepsObj: RegistrationStepsObject = {
    UsernameStep,
    EmailOrPhoneStep,
    PasswordStep,
    SmsCodeStep,
    AgreementStep
};

export const RegistrationPage = () => {
    const [CurrentComponent, setCurrentStep, goToPrevStep, currentStep] = useSteps<RegistrationStepName, SignStepProps>(
        stepsObj
    );

    // Unmount cleanup
    useEffect(
        () => () => {
            effects.changeChosenSignInput('');
        },
        []
    );

    const noMainWrapperVerticalPadding = currentStep === 'AgreementStep';

    return (
        <Formik {...registrationInitialProps}>
            <RegistrationWrapper>
                <TopBar onButtonClick={goToPrevStep} />
                <MainWrapper noVerticalPadding={noMainWrapperVerticalPadding}>
                    <CurrentComponent setCurrentStep={setCurrentStep} />
                </MainWrapper>
            </RegistrationWrapper>
        </Formik>
    );
};
