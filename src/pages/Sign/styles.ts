import styled, { css } from 'styled-components';

import { StyledInputWrapper } from 'components/common/Input/WrappedInput/styles';

export const StepTitle = styled.h1`
    font-size: 18px;
    margin-bottom: 23px;
    text-align: center;
    min-height: 52px;
    line-height: 26px;
`;

interface MainWrapperProps {
    noVerticalPadding?: boolean;
}

export const MainWrapper = styled.section<MainWrapperProps>`
    ${({ noVerticalPadding }) => css`
        padding: ${noVerticalPadding ? '0 40px' : '111px 40px 0'};
        min-height: 100%;
        display: flex;
        flex-direction: column;

        @media screen and (max-width: 400px) {
            padding: ${noVerticalPadding ? '0 20px' : '111px 20px 0'};
        }
    `}
`;

export const RegistrationWrapper = styled.div`
    height: calc(100% - 60px);
`;

export const InputsWrapper = styled.div`
    min-height: 165px;
    ${StyledInputWrapper}:not(:last-child) {
        margin-bottom: 14px;
    }
`;

export const NextButtonWrapper = styled.div`
    display: flex;
    justify-content: flex-end;
    margin: 35px 0 28px;
`;

export const Footnote = styled.div`
    text-align: center;
    font-size: 12px;
    color: #aeadad;
    margin-top: 25px;
    margin-bottom: -5px;
    position: relative;

    @media screen and (max-width: 400px) {
        margin-bottom: 10px;
    }
`;

export const FootnoteLink = styled.span`
    display: inline-block;
    border-bottom: 1px solid;
    cursor: pointer;
`;

export const NoMarginNextButtonWrapper = styled(NextButtonWrapper)`
    margin-top: 0;
`;
