import React, { Suspense } from 'react';
import { Fallback } from 'components/common/Fallback';
import { isSafari } from 'utils/mobile';
import { SafariFallback } from 'pages/VideoRecord/safariFallback';

const VideoRecord = React.lazy(() =>
    import('components/common/VideoRecord').then(module => ({ default: module.VideoRecord }))
);

export const VideoRecordPage = () => {
    // @ts-ignore
    const mediaRecorderEnable = typeof window.MediaRecorder === 'function';

    return (
        <Suspense fallback={<Fallback />}>
            {isSafari && !mediaRecorderEnable ? <SafariFallback /> : <VideoRecord />}
        </Suspense>
    );
};
