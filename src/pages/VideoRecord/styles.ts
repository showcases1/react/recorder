import styled from 'styled-components/macro';

export const SafariFallbackStyled = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    height: 100%;
    width: 100%;
`;
