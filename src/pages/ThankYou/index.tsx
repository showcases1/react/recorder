import React, { FC } from 'react';

import { Wrapper, Text } from 'pages/ThankYou/styles';

export const ThankYouPage: FC = () => (
    <Wrapper>
        <Text>Thank you</Text>
    </Wrapper>
);
