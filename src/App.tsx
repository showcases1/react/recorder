import React, { FC } from 'react';

import { Steps } from 'effects/steps';

import { GlobalStyle } from 'styles';

import { EntryPage } from 'pages/Entry';
import { VideoRecordPage } from 'pages/VideoRecord';
import { RegistrationPage } from 'pages/Sign/Registration';
import { LoginPage } from 'pages/Sign/Login';
import { PasswordForgottenPage } from 'pages/Sign/PasswordForgotten';
import { ConfirmationPage } from 'pages/Sign/Confirmation';
import { UploadPage } from 'pages/Upload';
import { ThankYouPage } from 'pages/ThankYou';
import { Layout } from 'components/Layout';
import { StepRoute } from 'components/StepRoute';
import { Notifications } from 'components/Notifications';

import 'fonts.css';

const App: FC = () => (
    <>
        <GlobalStyle />
        <Layout>
            <Notifications />
            <StepRoute step={Steps.Entry}>
                <EntryPage />
            </StepRoute>
            <StepRoute step={Steps.VideoRecord}>
                <VideoRecordPage />
            </StepRoute>
            <StepRoute step={Steps.Registration}>
                <RegistrationPage />
            </StepRoute>
            <StepRoute step={Steps.Login}>
                <LoginPage />
            </StepRoute>
            <StepRoute step={Steps.PasswordForgotten}>
                <PasswordForgottenPage />
            </StepRoute>
            <StepRoute step={Steps.Confirmation}>
                <ConfirmationPage />
            </StepRoute>
            <StepRoute step={Steps.Upload}>
                <UploadPage />
            </StepRoute>
            <StepRoute step={Steps.ThankYou}>
                <ThankYouPage />
            </StepRoute>
        </Layout>
    </>
);

export default App;
