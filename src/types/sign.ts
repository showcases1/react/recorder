import { FC } from 'react';

export interface SignStepProps {
    setCurrentStep(stepName: RegistrationStepName | LoginStepName | PasswordForgottenStepName): void;
}

export type RegistrationStepName =
    | 'UsernameStep'
    | 'EmailOrPhoneStep'
    | 'SmsCodeStep'
    | 'PasswordStep'
    | 'AgreementStep';
export type LoginStepName = 'EmailOrPhoneStep' | 'PasswordStep';
export type PasswordForgottenStepName = 'EmailStep' | 'PasswordAndCodeStep';

export type RegistrationStepsObject = {
    [stepName in RegistrationStepName]: FC<SignStepProps>;
};
export type LoginStepsObject = {
    [stepName in LoginStepName]: FC<SignStepProps>;
};
export type PasswordForgottenStepsObject = {
    [stepName in PasswordForgottenStepName]: FC<SignStepProps>;
};
