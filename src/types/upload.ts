import { FC } from 'react';

export interface UploadStepProps {
    setCurrentStep(stepName: UploadStepName): void;
    goToPrevStep(): void;
}

export type UploadStepName = 'TitleStep' | 'ProductsStep' | 'LanguageStep' | 'FinalStep';

export type UploadStepsObject = {
    [stepName in UploadStepName]: FC<UploadStepProps>;
};
