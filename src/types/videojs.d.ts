declare module 'video.js' {
    interface Record {
        // start record
        start: () => void;
        // stop record
        stop: () => void;
        // start loading data
        load: (url: Blob | string) => void;
    }

    interface Player extends Pick<HTMLMediaElement, 'pause' | 'play'> {
        // add event listener
        on: (event: string, handler: Function) => void;

        // destroy player
        dispose: () => void;
        deviceErrorCode: string;
        recordedData: Blob;

        record: () => Record;

        src: (settings: any) => void;

        currentTime: (time: number) => void;
        loop: (value: boolean) => void;
    }

    function video(video: HTMLVideoElement | null, options: any, callback?: Function): Player;

    export = video;
}
