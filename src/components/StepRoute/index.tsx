import { useStore } from 'effector-react';
import { state, Steps } from 'effects/steps';
import { ReactElement } from 'react';

interface Props {
    step: Steps;
    children: ReactElement;
}

export const StepRoute = ({ step, children }: Props) => {
    const currentStep = useStore(state.step);

    return step === currentStep ? children : null;
};
