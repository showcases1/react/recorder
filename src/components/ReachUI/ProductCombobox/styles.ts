import styled from 'styled-components';
import { Combobox } from '@reach/combobox';

import { reachSelectAndComboboxStyles } from 'components/ReachUI/styles';

import preloaderSvg from 'assets/preloader.svg';

export const Title = styled.div`
    font-size: 12px;
    margin: 8px 0 5px;
`;

export const StyledCombobox = styled(Combobox)`
    ${reachSelectAndComboboxStyles};
`;

export const NoFoundPlaceholder = styled.li`
    padding: 0 20px;
`;

export const Preloader = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 200px;
    &::before {
        content: url(${preloaderSvg});
        width: 30px;
        height: 30px;
    }
`;
