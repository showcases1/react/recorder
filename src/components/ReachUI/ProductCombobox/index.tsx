import { ComboboxInput, ComboboxList, ComboboxOption, ComboboxPopover } from '@reach/combobox';
import '@reach/combobox/styles.css';
import { NoFoundPlaceholder, Preloader, StyledCombobox, Title } from 'components/ReachUI/ProductCombobox/styles';
import { useStore } from 'effector-react';
import { effects as notificationsEffects } from 'effects/notifications';
import { effects as productEffects } from 'effects/product';
import { Form, useField } from 'formik';
import { useDebounce } from 'hooks';
import React, { ChangeEvent, Dispatch, FC, FormEvent, useCallback, useEffect, useState } from 'react';



interface Props {
    secondary: boolean;
    setIsAddingSecondaryProduct: Dispatch<boolean>;
}

export const ProductCombobox: FC<Props> = ({ secondary, setIsAddingSecondaryProduct }) => {
    const isLoading = useStore(productEffects.queryListOfProducts.pending);
    const [queriedProducts, setQueriedProducts] = useState<Recorder.ProductResponse[]>([]);
    const [isOptionsVisible, setIsOptionsVisible] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const debounce = useDebounce(350);

    const [, , mainProductHelpers] = useField('mainProduct');
    const [, secondaryProductsMeta, secondaryProductsHelpers] = useField('secondaryProducts');

    const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => setInputValue(e.target.value);

    const handleProductNameChange = useCallback(async () => {
        if (inputValue.length > 3) {
            setIsOptionsVisible(true);

            const response = await productEffects.queryListOfProducts(inputValue);

            if (response && response.items) {
                setQueriedProducts(response.items.slice(0, 5));
            }
        } else {
            setIsOptionsVisible(false);
        }
    }, [inputValue]);

    useEffect(() => {
        // Effect fires when inputValue is updated because
        // handleProductNameChange depends on inputValue changes
        debounce(handleProductNameChange);
    }, [debounce, handleProductNameChange]);

    const addProduct = (productName: string) => {
        let inputObject = queriedProducts.find(product => product.name === productName);

        if (!inputObject) {
            inputObject = {
                name: productName
            };
        }

        if (secondary) {
            const secondaryProductsArray = secondaryProductsMeta.value;
            const isInArray = secondaryProductsArray.some(
                (product: Recorder.ProductResponse) => product.name === productName
            );

            if (!isInArray) {
                secondaryProductsHelpers.setValue([...secondaryProductsArray, inputObject]);

                setIsAddingSecondaryProduct(false);

                setInputValue('');
            } else {
                notificationsEffects.setNotification({
                    message: 'Product already selected',
                    description: ''
                });
            }
        } else {
            mainProductHelpers.setValue(inputObject);

            setInputValue('');
        }
    };

    const handleComboboxSelect = (value: string) => {
        addProduct(value);
    };

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();

        if (inputValue.length > 1) {
            addProduct(inputValue);
        }
    };

    return (
        <Form onSubmit={handleFormSubmit}>
            <Title>Enter the brand and model</Title>
            <StyledCombobox openOnFocus onSelect={handleComboboxSelect}>
                <ComboboxInput
                    autoFocus={secondary}
                    placeholder={`+ add ${secondary ? 'secondary' : 'main'} product`}
                    value={inputValue}
                    onChange={handleInputChange}
                />
                {isOptionsVisible && (
                    <ComboboxPopover portal={false}>
                        <ComboboxList>
                            {isLoading ? (
                                <Preloader />
                            ) : queriedProducts.length ? (
                                queriedProducts.map(product => (
                                    <ComboboxOption key={product.id} value={product.name || ''} />
                                ))
                            ) : (
                                <NoFoundPlaceholder>
                                    No product found. Please complete the Brand and Product Name and press OK/Done to
                                    continue'
                                </NoFoundPlaceholder>
                            )}
                        </ComboboxList>
                    </ComboboxPopover>
                )}
            </StyledCombobox>
        </Form>
    );
};
