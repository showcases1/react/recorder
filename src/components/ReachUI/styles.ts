import { css } from 'styled-components';

export const reachSelectAndComboboxStyles = css`
    height: 60px;
    width: 100%;
    border-radius: 10px;
    background: #f4f4f4;
    color: #000;
    font-weight: 700;
    position: relative;
    outline: none;

    &::placeholder {
        font-weight: 400;
    }

    [data-reach-listbox-button],
    [data-reach-combobox-input] {
        width: 100%;
        height: 100%;
        border: none;
        padding: 0 20px;
        outline: none;
    }
    [data-reach-listbox-popover],
    [data-reach-combobox-popover] {
        width: 100%;
        padding: 10px 0;
        left: 0;
        background: none;
        border: none;
        outline: none;
        box-shadow: none;
    }
    [data-reach-listbox-list],
    [data-reach-combobox-list] {
        background: #f6f6f6;
        padding: 10px 0;
        box-shadow: 0 2px 8px -1px rgba(0, 0, 0, 0.4);
    }
    [data-reach-listbox-option],
    [data-reach-combobox-option] {
        height: 40px;
        padding: 0 20px;
        display: flex;
        align-items: center;
        font-size: 16px;
        color: #000;
        &[data-current] {
            background: none;
            font-weight: bold;
        }
        &[aria-selected='true'] {
            background: hsl(211, 10%, 92%);
        }
    }
    [data-reach-listbox-arrow] {
        font-size: 13px;
        &[data-expanded] {
            transform: rotate(180deg);
        }
    }
`;
