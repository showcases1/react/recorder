import styled from 'styled-components';
import { Listbox } from '@reach/listbox';

import { reachSelectAndComboboxStyles } from 'components/ReachUI/styles';

export const StyledSelect = styled(Listbox)`
    ${reachSelectAndComboboxStyles};
`;
