import React, { FC } from 'react';
import { useField } from 'formik';
import { ListboxOption } from '@reach/listbox';
import '@reach/listbox/styles.css';

import { StyledSelect } from 'components/ReachUI/Select/styles';

interface Props {
    values: string[];
    name: string;
}

export const Select: FC<Props> = ({ values, name }) => {
    const [field, , helpers] = useField(name);

    const handleChange = (value: string) => {
        helpers.setValue(value);
    };

    return (
        <StyledSelect arrow="V" {...field} portal={false} onChange={handleChange}>
            {values.map((value: string) => (
                <ListboxOption key={value} value={value}>
                    {value}
                </ListboxOption>
            ))}
        </StyledSelect>
    );
};
