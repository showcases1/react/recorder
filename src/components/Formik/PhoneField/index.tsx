import React, { FC } from 'react';
import { useField } from 'formik';

import { effects } from 'effects/sign';
import { useEffectsPendingStatus } from 'hooks';

import { PhoneInput } from 'components/common/Input/PhoneInput';
import { WrappedInput } from 'components/common/Input/WrappedInput';

interface Props {
    onFocus?(): void;
    withLeftHelper?: boolean;
}

export const PhoneField: FC<Props> = ({ onFocus, withLeftHelper }) => {
    const isLoading = useEffectsPendingStatus(effects.analyzeMobileNumber);
    const [field, meta] = useField('phone');

    const chooseEmail = () => {
        effects.changeChosenSignInput('email');
    };

    const { error } = meta;
    const topHelper = error ? error : 'Looks good, let’s keep going 😁';
    const leftHelper = <div onClick={chooseEmail}>Use email</div>;

    return (
        <WrappedInput
            {...field}
            as={PhoneInput}
            isLoading={isLoading}
            label="Phone"
            leftHelper={withLeftHelper && leftHelper}
            topHelper={topHelper}
            onFocus={onFocus}
        />
    );
};
