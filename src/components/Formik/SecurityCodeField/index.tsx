import React, { FC } from 'react';
import { useField } from 'formik';

import { WrappedInput } from 'components/common/Input/WrappedInput';

export const SecurityCodeField: FC = () => {
    const [field, meta] = useField('securityCode');
    const { error } = meta;

    return <WrappedInput {...field} label="Security code" topHelper={error} />;
};
