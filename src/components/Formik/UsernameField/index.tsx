import React, { FC } from 'react';
import { useField } from 'formik';

import { effects } from 'effects/sign';
import { useEffectsPendingStatus } from 'hooks';

import { WrappedInput } from 'components/common/Input/WrappedInput';

export const UsernameField: FC = () => {
    const isLoading = useEffectsPendingStatus(effects.checkUsernameExistence);
    const [field, meta] = useField('username');

    let { value } = field;
    const { error } = meta;

    if (value && value[0] !== '@') {
        value = '@' + value;
    } else if (value === '@') {
        value = '';
    }

    const topHelper = error ? error : 'Oh yeah, that will so totally work 😁';

    return (
        <WrappedInput
            {...field}
            isLoading={isLoading}
            label="Name"
            placeholder="@yoursuperusername"
            topHelper={topHelper}
            type="text"
            value={value}
        />
    );
};
