import React, { FC } from 'react';
import { useField } from 'formik';

import { effects as stepsEffects, Steps } from 'effects/steps';
import { effects as signEffects } from 'effects/sign';

import { WrappedInput } from 'components/common/Input/WrappedInput';

interface Props {
    onFocus?(): void;
    login?: boolean;
    isLoading?: boolean;
    withLeftHelper?: boolean;
}

export const EmailField: FC<Props> = ({ onFocus, login, isLoading, withLeftHelper }) => {
    const [field, meta] = useField('email');

    const choosePhone = () => {
        signEffects.changeChosenSignInput('phone');
    };

    const goToForgottenPasswordPage = () => {
        stepsEffects.setStep(Steps.PasswordForgotten);
    };

    const { error } = meta;
    const topHelper = error ? error : 'Looks good, let’s keep going 😁';
    const leftHelper = <div onClick={choosePhone}>Use phone</div>;
    const rightHelper = <div onClick={goToForgottenPasswordPage}>Forgot your password?</div>;

    return (
        <WrappedInput
            {...field}
            isLoading={isLoading}
            label="Email"
            leftHelper={withLeftHelper && leftHelper}
            rightHelper={login && rightHelper}
            topHelper={topHelper}
            type="email"
            onFocus={onFocus}
        />
    );
};
