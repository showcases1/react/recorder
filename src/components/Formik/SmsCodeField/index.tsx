import React, { FC } from 'react';
import { useField } from 'formik';

import { effects } from 'effects/sign';
import { useEffectsPendingStatus } from 'hooks';

import { UnderLined } from 'components/Formik/SmsCodeField/styles';
import { WrappedInput } from 'components/common/Input/WrappedInput';

export const SmsCodeField: FC = () => {
    const isCheckPending = useEffectsPendingStatus(effects.checkSmsCode);
    const isSendPending = useEffectsPendingStatus(effects.sendSmsCode);
    const isLoading = isCheckPending || isSendPending;

    const [smsCodeField, smsCodeMeta, smsCodeHelpers] = useField('smsCode');
    const [, countryCodeMeta] = useField('countryCode');
    const [, phoneMeta] = useField('phone');

    const sendNewCode = async () => {
        await effects.sendSmsCode(countryCodeMeta.value + phoneMeta.value);

        smsCodeHelpers.setValue('');
    };

    const { error } = smsCodeMeta;
    let topHelper;

    if (!error) {
        topHelper = 'That looks about right, so let’s go 😁';
    } else if (error === 'Invalid') {
        topHelper = (
            <span>
                Well, that didn’t work. Shall we <UnderLined onClick={sendNewCode}>send a new code?</UnderLined>
            </span>
        );
    } else {
        topHelper = error;
    }

    return <WrappedInput {...smsCodeField} isLoading={isLoading} label="Security code" topHelper={topHelper} />;
};
