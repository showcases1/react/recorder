import styled from 'styled-components';

export const UnderLined = styled.span`
    display: inline-block;
    border-bottom: 1px solid;
    cursor: pointer;
`;
