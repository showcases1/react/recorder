import React, { FC } from 'react';
import { useField } from 'formik';

import { WrappedInput } from 'components/common/Input/WrappedInput';

export const TitleField: FC = () => {
    const [field, meta] = useField('title');
    const { error } = meta;

    return <WrappedInput {...field} label="Title" topHelper={error} />;
};
