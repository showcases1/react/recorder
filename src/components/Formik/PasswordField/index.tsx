import React, { FC } from 'react';
import { useField } from 'formik';

import { effects, Steps } from 'effects/steps';

import { WrappedInput } from 'components/common/Input/WrappedInput';

interface Props {
    login?: boolean;
    forgotten?: boolean;
    isLoading?: boolean;
}

export const PasswordField: FC<Props> = ({ login, forgotten, isLoading }) => {
    const [field, meta] = useField('password');

    const goToForgottenPasswordPage = () => {
        effects.setStep(Steps.PasswordForgotten);
    };

    const { error } = meta;
    const topHelper = error || login || forgotten ? error : 'That looks nice and STRONG, so let’s go 😁';
    const rightHelper = <div onClick={goToForgottenPasswordPage}>Forgot your password?</div>;

    return (
        <WrappedInput
            {...field}
            autoFocus={!forgotten}
            isLoading={isLoading}
            label="Password"
            placeholder={forgotten ? 'Enter your new password' : 'Type your password'}
            rightHelper={login && rightHelper}
            topHelper={topHelper}
            type="password"
        />
    );
};
