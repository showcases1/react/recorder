import React, { FC } from 'react';

import { StyledTopBar, TopBarTitle } from 'components/common/TopBar/styles';
import { NavigationButton } from 'components/common/NavigationButton';

interface Props {
    onButtonClick: () => void;
    title?: string;
}

export const TopBar: FC<Props> = ({ onButtonClick, title }) => (
    <StyledTopBar>
        <NavigationButton onClick={onButtonClick} />
        {title && <TopBarTitle>{title}</TopBarTitle>}
    </StyledTopBar>
);
