import styled from 'styled-components';
import { StyledNavigationButton } from 'components/common/NavigationButton/styles';

export const StyledTopBar = styled.header`
    padding-top: 28px;
    height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    ${StyledNavigationButton} {
        position: absolute;
        left: 8px;
    }
`;

export const TopBarTitle = styled.h2`
    font-size: 16px;
`;
