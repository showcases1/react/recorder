import React, { ChangeEvent, FC, InputHTMLAttributes } from 'react';

import { StyledInput } from './styles';

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    value: string;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const Input: FC<InputProps> = props => <StyledInput {...props} />;
