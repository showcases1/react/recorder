import React, { FC, useEffect, useCallback } from 'react';
import { useField } from 'formik';
import ReactPhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

import { PhoneInputWrapper } from './styles';

import { InputProps } from '../index';

const codeContainerClass = 'country-code-container';
const phoneInputClass = 'country-code-input';

export const PhoneInput: FC<InputProps> = ({ value, ...rest }) => {
    const [, countryCodeMeta, countryCodeHelpers] = useField('countryCode');
    const [, countryIdMeta, countryIdHelpers] = useField('countryId');
    const countryCode = countryCodeMeta.value;
    const formattedValue = value.replace(`${countryCode}`, '');

    // Original component has country code inside input
    // but not inside dropdown button. This function
    // puts code in dropdown button and properly styles component.
    const addCodeToContainer = useCallback((code: string) => {
        const codeContainer = document.querySelector<HTMLElement>(`.${codeContainerClass}`);
        const inputElement = document.querySelector<HTMLElement>(`.${phoneInputClass}`);

        if (!codeContainer || !inputElement) return;

        // form-control class prevents
        // form submit by pressing enter
        inputElement.classList.remove('form-control');

        const flagElement = codeContainer.querySelector<HTMLElement>('.flag');
        let codeElement = codeContainer.querySelector<HTMLElement>('.country-code');

        if (!flagElement) return;

        if (!codeElement) {
            codeElement = document.createElement('div');
            codeElement.classList.add('country-code');
        }
        codeElement.innerText = `${code}`;

        flagElement.prepend(codeElement);

        const containerWidth = 38 + codeElement.clientWidth;

        codeContainer.style.width = `${containerWidth}px`;
        inputElement.style.paddingLeft = `${containerWidth + 5}px`;
    }, []);

    useEffect(() => {
        if (countryCode) {
            addCodeToContainer(countryCode);
        }
    }, [countryCode, addCodeToContainer]);

    const handleValidation = (newCountryCode: string, country: object) => {
        const countryObject = country as { iso2: string };

        // Country code fallback
        if (!newCountryCode) {
            countryIdHelpers.setValue('us');

            return true;
        }

        // Fires addCodeToContainer() when rendered
        if (`+${newCountryCode}` !== countryCode) {
            // TODO: error
            countryCodeHelpers.setValue(`+${newCountryCode}`);
            countryIdHelpers.setValue(countryObject.iso2);
        }

        // If no return there is error
        return true;
    };

    return (
        <PhoneInputWrapper>
            <ReactPhoneInput
                buttonClass={codeContainerClass}
                country={countryIdMeta.value}
                inputClass={phoneInputClass}
                inputProps={{
                    ...rest,
                    type: 'tel',
                    placeholder: '',
                    value: formattedValue
                }}
                isValid={handleValidation}
            />
        </PhoneInputWrapper>
    );
};
