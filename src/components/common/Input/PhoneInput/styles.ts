import styled from 'styled-components';

import { inputStyle } from '../styles';

export const PhoneInputWrapper = styled.div`
    .react-tel-input {
        font-family: 'SF UI Text', sans-serif;
        font-size: 16px;
    }
    .country-code-input {
        border: none;
        ${inputStyle};
        font-size: 16px;
        border-radius: 0;
        height: auto;
    }
    .country-code-container {
        background: none;
        bottom: 3px;
        border-width: 2px;
        border-right: none;
        padding-bottom: 1px;
        .country-list .country {
            display: flex;
        }
        .selected-flag {
            width: 100%;
        }
        .flag {
            display: flex;
            align-items: center;
            width: 16px !important;
            .arrow {
                position: static;
                margin-top: 0;
            }
            .country-code {
                margin-left: 16px;
                padding: 0 6px;
                font-size: 16px;
            }
        }
    }
`;
