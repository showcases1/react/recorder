import styled, { css } from 'styled-components';

export const inputStyle = css`
    line-height: 20px;
    padding: 8px 0;
    border-bottom: 3px solid #00ff44;
    width: 100%;
    &::placeholder {
        color: rgba(#3c3c3c, 0.5);
        font-weight: 400;
    }
    &:-ms-input-placeholder {
        color: rgba(#3c3c3c, 0.5);
        font-weight: 400;
    }
    // Making autofill background in Chrome white
    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus,
    &:-webkit-autofill:active {
        -webkit-box-shadow: 0 0 0 30px white inset;
    }
`;

export const StyledInput = styled.input`
    ${inputStyle};
`;
