import React, { FC, ReactNode } from 'react';

import {
    BottomHelpersWrapper,
    Label,
    LeftHelper,
    Preloader,
    PreloaderWrapper,
    RightHelper,
    StyledInputWrapper,
    TopHelper
} from 'components/common/Input/WrappedInput/styles';
import { Input, InputProps } from 'components/common/Input/index';

interface Props extends InputProps {
    topHelper?: string | ReactNode;
    leftHelper?: string | ReactNode;
    rightHelper?: string | ReactNode;
    className?: string;
    isLoading?: boolean;
    label: string;
    as?: FC<InputProps>;
}

export const WrappedInput: FC<Props> = ({
    label,
    topHelper,
    leftHelper,
    rightHelper,
    isLoading,
    className,
    as,
    ...rest
}) => {
    const InputComponent = as ? as : Input;

    return (
        <StyledInputWrapper className={className}>
            <Label>{label}</Label>
            <PreloaderWrapper>
                <InputComponent {...rest} />
                {isLoading && <Preloader />}
            </PreloaderWrapper>
            {topHelper && <TopHelper>{topHelper}</TopHelper>}
            <BottomHelpersWrapper>
                <LeftHelper>{leftHelper}</LeftHelper>
                <RightHelper>{rightHelper}</RightHelper>
            </BottomHelpersWrapper>
        </StyledInputWrapper>
    );
};
