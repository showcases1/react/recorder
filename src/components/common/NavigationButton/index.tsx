import React, { FC } from 'react';
import { StyledNavigationButton, StyledNavigationButtonProps } from './styles';

interface Props extends StyledNavigationButtonProps {
    onClick?: () => void;
    submit?: boolean;
    className?: string;
}

export const NavigationButton: FC<Props> = ({ submit, ...rest }) => (
    <StyledNavigationButton type={submit ? 'submit' : 'button'} {...rest} />
);
