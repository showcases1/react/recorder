import styled, { css } from 'styled-components';

import backArrow from 'assets/icons/backArrow.svg';
import circleForwardArrow from 'assets/icons/circleForwardArrow.svg';
import preloaderSvgWhite from 'assets/preloader-white.svg';

export interface StyledNavigationButtonProps {
    circle?: boolean | string;
    disabled?: boolean;
    isLoading?: boolean;
}

export const StyledNavigationButton = styled.button<StyledNavigationButtonProps>`
    ${({ circle, disabled, isLoading }) => css`
        background: url(${backArrow}) center no-repeat;
        width: 32px;
        height: 32px;
        cursor: pointer;
        ${circle &&
        css`
            background-image: url(${isLoading ? preloaderSvgWhite : circleForwardArrow});
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background-color: #00ff44;
            background-size: ${isLoading ? '20px' : '16px'};
        `}
        ${disabled &&
        css`
            background-color: #d8d8d8;
            cursor: default;
        `}
    `}
`;
