import { FallbackStyled } from 'components/common/Fallback/styles';
import React from 'react';

export const Fallback = () => <FallbackStyled>Loading...</FallbackStyled>;
