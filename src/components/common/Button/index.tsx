import React, { FC } from 'react';

import { StyledButton, StyledButtonProps } from './styles';

import preloaderSvgGreen from 'assets/preloader.svg';
import preloaderSvgWhite from 'assets/preloader-white.svg';

interface Props extends StyledButtonProps {
    onClick: () => void;
    className?: string;
    isLoading?: boolean;
}

export const Button: FC<Props> = ({ children, isLoading, view, ...rest }) => (
    <StyledButton view={view} {...rest}>
        {isLoading ? (
            view === 'green' ? (
                <img alt="" src={preloaderSvgWhite} />
            ) : (
                <img alt="" src={preloaderSvgGreen} />
            )
        ) : (
            children
        )}
    </StyledButton>
);
