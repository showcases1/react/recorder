import styled, { css } from 'styled-components';

export interface StyledButtonProps {
    view: 'inline' | 'green' | 'white' | 'grey' | 'dark' | 'ghost';
    disabled?: boolean;
    width?: string;
}

export const StyledButton = styled.button<StyledButtonProps>`
    width: ${props => props.width || '100%'};
    height: 52px;
    padding: 15px 0;
    border-radius: 100px;
    font-size: 15px;
    line-height: 0;
    cursor: pointer;
    font-weight: 700;
    flex: none;

    img {
        height: 100%;
    }

    ${({ view }) => {
        switch (view) {
            case 'inline':
                return css`
                    height: 21px;
                    width: auto;
                    border-radius: 0;
                    color: #000;
                    font-size: 16px;
                `;
            case 'green':
                return css`
                    background: #00ff44;
                `;
            case 'white':
                return css`
                    background: #fff;
                `;
            case 'grey':
                return css`
                    background: #d8d8d8;
                `;
            case 'dark':
                return css`
                    background: #3c3c3c;
                    color: #fff;
                `;
            case 'ghost':
                return css`
                    background: none;
                    border: 2px solid #fff;
                    color: #fff;
                `;
        }
    }}
`;
