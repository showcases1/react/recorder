import React, { FC, ReactEventHandler } from 'react';
import { Icon, StyledIconProps } from './styles';

interface Props extends StyledIconProps {
    onClick: ReactEventHandler;
}

export const StartButton: FC<Props> = ({ onClick, type = 'start' }) => <Icon type={type} onClick={onClick} />;
