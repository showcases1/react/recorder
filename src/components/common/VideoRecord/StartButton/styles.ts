import styled from 'styled-components/macro';

import StartRecordIcon from 'assets/icons/start-record.svg';
import StopRecordIcon from 'assets/icons/stop-record.svg';

export interface StyledIconProps {
    type?: 'start' | 'stop';
}

export const Icon = styled.div<StyledIconProps>`
    position: absolute;

    bottom: 72px;
    left: calc(50% - 32px);

    width: 64px;
    height: 64px;

    background-image: ${props => (props.type === 'start' ? `url(${StartRecordIcon})` : `url(${StopRecordIcon})`)};

    cursor: pointer;
`;
