import styled from 'styled-components/macro';

export const Wrapper = styled.div`
    position: absolute;
    bottom: 120px;
    left: 20px;

    width: calc(100% - 40px);
`;

export const VideoFrames = styled.div`
    display: flex;
`;

export const VideoFrame = styled.div`
    position: relative;

    flex: 1 1 0%;

    min-width: 0;

    height: 50px;
`;

export const FrameImg = styled.img`
    position: absolute;
    top: 50%;
    left: 50%;

    width: 100%;
    height: 100%;

    transform: translate(-50%, -50%);

    object-fit: cover;
`;

export const LoadingFramesTitle = styled.p`
    font-size: 16px;
    color: white;

    text-align: center;
`;
