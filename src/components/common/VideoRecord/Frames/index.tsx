import React, { FC, useEffect, useState } from 'react';
import { Wrapper, FrameImg, VideoFrame, VideoFrames, LoadingFramesTitle } from './styles';
import { Slider, SliderProps } from './Slider';
import { getBlobDuration, getFrame } from 'utils/video';

interface Props extends Omit<SliderProps, 'duration'> {
    video: Blob;
}

const FRAME_COUNT = 10;

/**
 * Component to create video frames timeline
 */
export const Frames: FC<Props> = ({ video, onChange, onSlideStart }) => {
    const [images, setImages] = useState<string[]>([]);
    const [duration, setDuration] = useState(0);
    const [videoCopy, setVideoCopy] = useState<HTMLVideoElement>();
    const [ready, setReady] = useState(false);

    useEffect(() => {
        const getDuration = async () => {
            const { video: videoEl, duration } = await getBlobDuration(video);

            setDuration(duration);
            setVideoCopy(videoEl);
        };

        getDuration();
    }, [video]);

    useEffect(() => {
        const getFrames = async () => {
            if (!duration || isNaN(duration) || !isFinite(duration) || !videoCopy) return;

            // to await changed video time
            let seekResolve: Function;

            videoCopy.onseeked = async () => {
                if (typeof seekResolve === 'function') {
                    seekResolve();
                }
            };

            const array = Array.from({ length: FRAME_COUNT }, (_el, idx) => idx);
            const images = [];

            // get frames from video
            for (const idx of array) {
                videoCopy.currentTime = Math.round((idx * duration) / FRAME_COUNT);

                // eslint-disable-next-line no-loop-func
                await new Promise(res => (seekResolve = res));

                const image = getFrame(videoCopy);

                images.push(image);
            }

            setImages(images);
            setReady(true);
        };

        getFrames();

        return () => {
            setImages([]);
        };
    }, [duration, videoCopy]);

    return (
        <Wrapper>
            {ready ? (
                <>
                    <Slider duration={duration} onChange={onChange} onSlideStart={onSlideStart} />
                    <VideoFrames>
                        {images.map((image, idx) => (
                            <VideoFrame key={idx.toString()}>
                                <FrameImg alt="" src={image} />
                            </VideoFrame>
                        ))}
                    </VideoFrames>
                </>
            ) : (
                <LoadingFramesTitle>Loading video frames</LoadingFramesTitle>
            )}
        </Wrapper>
    );
};
