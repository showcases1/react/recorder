import styled from 'styled-components/macro';
import { Slider, SliderItem } from 'react-compound-slider';

interface HandleProps {
    handle: SliderItem;
}

interface TrackProps {
    source: SliderItem;
    target: SliderItem;
}

export const StyledSlider = styled(Slider)`
    position: absolute;
    top: -4px;

    width: 100%;
    height: 58px;
`;

export const Rail = styled.div`
    position: absolute;

    width: 100%;
    height: 100%;

    background-color: transparent;
`;

export const Handle = styled.div.attrs<HandleProps>(({ handle: { percent } }) => ({
    style: {
        left: `${percent}%`
    }
}))<HandleProps>`
    position: absolute;

    width: 4px;
    height: 100%;

    background-color: #00fe43;

    z-index: 2;

    cursor: e-resize;
`;

export const Track = styled.div.attrs<TrackProps>(({ source, target }) => ({
    style: {
        left: `${source.percent}%`,
        width: `${target.percent - source.percent}%`
    }
}))<TrackProps>`
    position: absolute;

    height: 100%;

    border-top: 4px solid #00fe43;
    border-bottom: 4px solid #00fe43;

    z-index: 1;

    cursor: pointer;
`;
