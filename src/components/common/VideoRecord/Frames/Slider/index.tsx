import React, { FC } from 'react';
import { Handles, Tracks } from 'react-compound-slider';
import { StyledSlider, Rail, Handle, Track } from './styles';

export interface SliderProps {
    onSlideStart: () => void;
    onChange: (values: readonly number[]) => void;
    duration: number;
}

export const Slider: FC<SliderProps> = ({ duration, onSlideStart, onChange }) => (
    <StyledSlider
        domain={[0, duration]}
        mode={2}
        step={duration / 500}
        values={[0, duration]}
        onChange={onChange}
        onSlideStart={onSlideStart}
    >
        <Rail />
        <Handles>
            {({ handles, getHandleProps }) => (
                <div className="slider-handles">
                    {handles.map(handle => (
                        <Handle key={handle.id} handle={handle} {...getHandleProps(handle.id)} />
                    ))}
                </div>
            )}
        </Handles>
        <Tracks left={false} right={false}>
            {({ tracks, getTrackProps }) => (
                <div className="slider-tracks">
                    {tracks.map(({ id, source, target }) => (
                        <Track key={id} source={source} target={target} {...getTrackProps()} />
                    ))}
                </div>
            )}
        </Tracks>
    </StyledSlider>
);
