import styled from 'styled-components/macro';

import StartRecordIcon from 'assets/icons/switch-camera.svg';

export const Icon = styled.div`
    position: absolute;

    top: 30px;
    left: calc(50% - 23px);

    width: 46px;
    height: 46px;

    background-image: url(${StartRecordIcon});

    cursor: pointer;
`;
