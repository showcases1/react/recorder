import React, { FC, ReactEventHandler } from 'react';
import { Icon } from './styles';

interface Props {
    onClick: ReactEventHandler;
}

export const SwitchCameraButton: FC<Props> = ({ onClick }) => <Icon onClick={onClick} />;
