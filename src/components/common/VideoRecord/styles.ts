import styled from 'styled-components/macro';
import PlayVideoIcon from 'assets/icons/video-play-button.svg';

export const VideoWrapper = styled.div`
    position: relative;

    width: 100%;
    height: 100%;

    background-color: black;

    overflow: hidden;

    .video-js {
        width: 100%;
        height: 100%;

        video {
            object-position: top;
        }
    }

    .vjs-record-indicator {
        display: none;
    }
`;

export const PlayButton = styled.div`
    position: absolute;

    top: 50%;
    left: 50%;

    width: 50px;
    height: 50px;

    background-image: url(${PlayVideoIcon});

    transform: translate(-50%, -50%);

    cursor: pointer;
`;
