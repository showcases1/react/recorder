import { StyledTimerProps, Line, StyledTimer, Button } from './styles';
import React, { FC, useState } from 'react';
import { CountdownTimer } from './CountdownTimer';
import { useToggle } from 'hooks';

interface Props extends StyledTimerProps {
    onSelectTime: () => void;
    onClose: () => void;
    onTimeEnd: () => void;
}

export const TimerPopup: FC<Props> = ({ open, onSelectTime, onClose, onTimeEnd }) => {
    const [showCountdown, toggleCountdown] = useToggle();
    const [countdownTime, setCountdownTime] = useState(0);

    const handleTimerStart = (time: number) => {
        toggleCountdown();

        setCountdownTime(time);

        onSelectTime();
    };

    const handleCountdownEnd = () => {
        toggleCountdown();

        onTimeEnd();
    };

    return (
        <>
            <StyledTimer open={open}>
                <Line onClick={onClose} />
                <Button onClick={() => handleTimerStart(5)}>5 seconds</Button>
                <Button onClick={() => handleTimerStart(10)}>10 seconds</Button>
            </StyledTimer>

            {showCountdown && <CountdownTimer time={countdownTime} onTimeEnd={handleCountdownEnd} />}
        </>
    );
};
