import styled from 'styled-components/macro';

export interface StyledTimerProps {
    open: boolean;
}

export const StyledTimer = styled.div<StyledTimerProps>`
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;

    width: 100%;
    height: 210px;

    background-color: white;

    border-top-left-radius: 20px;
    border-top-right-radius: 20px;

    transform: ${props => (props.open ? 'translateY(0)' : 'translateY(100%)')};

    transition: all 0.3s;
`;

export const Line = styled.div`
    width: 54px;
    height: 1px;

    margin: 10px auto 40px;

    border: 3px solid #7b7b7b;
    border-radius: 25px;
`;

export const Button = styled.div`
    width: 325px;
    height: 52px;

    max-width: 100%;

    margin: 0 auto 10px;

    text-align: center;
    font-size: 15px;
    line-height: 52px;
    color: black;

    background: linear-gradient(0deg, #d8d8d8, #d8d8d8), linear-gradient(0deg, #273c73, #273c73);

    border-radius: 100px;

    outline: none;

    cursor: pointer;
`;
