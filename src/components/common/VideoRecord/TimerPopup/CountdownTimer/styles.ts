import styled from 'styled-components/macro';

export const Timer = styled.div`
    position: absolute;

    top: 50%;
    left: 50%;

    font-size: 200px;
    color: white;

    transform: translate(-50%, -50%);
`;
