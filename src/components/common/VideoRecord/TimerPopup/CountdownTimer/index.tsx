import React, { FC, useEffect, useRef, useState } from 'react';
import { Timer } from 'components/common/VideoRecord/TimerPopup/CountdownTimer/styles';

interface Props {
    time: number;
    onTimeEnd: () => void;
}

export const CountdownTimer: FC<Props> = ({ time, onTimeEnd }) => {
    const [currentTime, setCurrentTime] = useState(time);
    const timerTimeout = useRef<number>();

    useEffect(() => {
        const runTimer = () => {
            timerTimeout.current = setInterval(() => {
                setCurrentTime(current => --current);
            }, 1000);
        };

        runTimer();

        return () => clearInterval(timerTimeout.current);
    }, []);

    useEffect(() => {
        if (currentTime === 0) {
            clearInterval(timerTimeout.current);

            onTimeEnd();
        }
    }, [currentTime, time, onTimeEnd]);

    return <Timer>{currentTime}</Timer>;
};
