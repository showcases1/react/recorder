/**
 * See doc about videojs-record
 * https://collab-project.github.io/videojs-record/#/
 */
import { SwitchCameraButton } from 'components/common/VideoRecord/SwitchCameraButton';
import { TimerButton } from 'components/common/VideoRecord/TimerButton';
import { TimerPopup } from 'components/common/VideoRecord/TimerPopup';
import { VideoViewer } from 'components/common/VideoViewer';
import { useStore } from 'effector-react';
import { effects as notificationsEffects } from 'effects/notifications';
import { effects as videoEffects, state } from 'effects/video';
import { useToggle } from 'hooks';
import React, { FC, useEffect, useRef, useState } from 'react';
import { isSafari } from 'utils/mobile';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';
import 'videojs-record/dist/css/videojs.record.css';
import 'videojs-record/dist/videojs.record.js';
import 'webrtc-adapter';
import { StartButton } from './StartButton';
import { VideoWrapper } from './styles';

enum FacingMode {
    User = 'user',
    Env = 'environment'
}

const getVideoOptions = (facingMode: FacingMode) => ({
    controls: false,
    autoplay: 'any',
    plugins: {
        record: {
            audio: {
                echoCancellation: false,
                noiseSuppression: true
            },
            video: {
                facingMode,
                width: 1280,
                height: 720
            },
            frameWidth: 1280,
            frameHeight: 720,
            // videoMimeType: 'video/webm', // TODO: попробовать Safari
            // videoMimeType: 'video/mp4', // TODO: РАБОТАЕТ в SAFARI
            videoMimeType: isSafari ? 'video/mp4' : 'video/webm', // TODO: проверить звук в SAFARI
            maxLength: 60, // max record time
            // debug: process.env.NODE_ENV === `development`
            debug: true
        }
    }
});

export const VideoRecord: FC = () => {
    const videoRef = useRef<HTMLVideoElement>(null);
    const [player, setPlayer] = useState<ReturnType<typeof videojs>>();
    const [ready, toggleReady] = useToggle();
    const [recording, toggleRecording] = useToggle();
    const [openTimer, toggleTimer] = useToggle();
    const [showCountdown, toggleCountdown] = useToggle();
    const [finishRecord, toggleFinishRecord] = useToggle();
    const [facingMode, setFacingMode] = useState<FacingMode>(FacingMode.User);
    const [switchingCamera, setSwitchingCamera] = useState(false);
    const [forceUpdate, setForceUpdate] = useState(0);
    const recordedVideo = useStore(state.recorderVideo);

    useEffect(() => {
        const videoPlayer = videojs(videoRef.current, getVideoOptions(facingMode));

        videoPlayer.on('deviceReady', toggleReady);

        videoPlayer.on('stopRecord', toggleRecording);

        // user clicked the record button and started recording
        videoPlayer.on('startRecord', toggleRecording);

        // user completed recording and stream is available
        videoPlayer.on('finishRecord', async () => {
            toggleFinishRecord();

            videoPlayer.loop(true);

            videoEffects.setRecorderVideo(videoPlayer.recordedData);
        });

        videoPlayer.on('error', (_element: any, error: any) => {
            notificationsEffects.setNotification({
                message: 'Error',
                description: error.toString()
            });
        });

        videoPlayer.on('deviceError', () => {
            notificationsEffects.setNotification({
                message: 'Device Error',
                description: videoPlayer.deviceErrorCode.toString()
            });
        });

        setPlayer(videoPlayer);

        // TODO: не все очищается при переходе на следующий шаг (frame отображают картинки)
        return () => videoPlayer.dispose();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [forceUpdate]);

    // start/stop record
    const handleRecord = () => {
        const record = player?.record();

        if (recording) {
            record?.stop();
        } else {
            record?.start();
        }
    };

    // refresh page (step back) or change camera
    const handleRefresh = (changeFacingMode = true) => {
        setSwitchingCamera(true);

        // TODO: SIMPLIFY THIS!!!
        // setTimeout to reset videojs player
        setTimeout(() => {
            setSwitchingCamera(false);
            toggleReady();
            videoEffects.setRecorderVideo(null);

            if (finishRecord) {
                toggleFinishRecord();
            }

            // we need to await render basic layout
            setTimeout(() => {
                if (changeFacingMode) {
                    setFacingMode(facingMode === FacingMode.User ? FacingMode.Env : FacingMode.User);
                }

                setForceUpdate(val => ++val);
            }, 0);
        }, 0);
    };

    const showStartButton = ready && !showCountdown && !finishRecord;
    const showTimerButton = showStartButton && !recording;
    const showSwitchButton = showStartButton && !recording;

    if (switchingCamera) return null;

    if (finishRecord && recordedVideo)
        return <VideoViewer recordedData={recordedVideo} onNavigationBack={() => handleRefresh(false)} />;

    return (
        <VideoWrapper>
            <div data-vjs-player="">
                <video ref={videoRef} playsInline className="video-js" />
            </div>

            {showSwitchButton && <SwitchCameraButton onClick={() => handleRefresh()} />}

            {showTimerButton && <TimerButton onClick={toggleTimer} />}

            {showStartButton && <StartButton type={recording ? 'stop' : 'start'} onClick={handleRecord} />}

            <TimerPopup
                open={openTimer}
                onClose={toggleTimer}
                onSelectTime={() => {
                    toggleTimer();
                    toggleCountdown();
                }}
                onTimeEnd={() => {
                    handleRecord();
                    toggleCountdown();
                }}
            />
        </VideoWrapper>
    );
};
