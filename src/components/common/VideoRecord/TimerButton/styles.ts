import styled from 'styled-components/macro';
import TimerIcon from 'assets/icons/timer.svg';

export const Icon = styled.div`
    position: absolute;

    bottom: 84px;
    left: 61px;

    width: 40px;
    height: 40px;

    background-image: url(${TimerIcon});

    cursor: pointer;
`;
