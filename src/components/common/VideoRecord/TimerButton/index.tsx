import { Icon } from 'components/common/VideoRecord/TimerButton/styles';
import React, { FC, ReactEventHandler } from 'react';

interface Props {
    onClick: ReactEventHandler;
}

export const TimerButton: FC<Props> = ({ onClick }) => <Icon onClick={onClick} />;
