/**
 * See doc about videojs-record
 * https://collab-project.github.io/videojs-record/#/
 */
import { Frames } from 'components/common/VideoRecord/Frames';
import { VideoWrapper } from 'components/common/VideoRecord/styles';
import { Navigation, NavigationBack, PlayButton } from 'components/common/VideoViewer/styles';
import { effects as stepEffects, Steps } from 'effects/steps';
import { effects as videoEffects } from 'effects/video';
import { useToggle } from 'hooks';
import React, { FC, useEffect, useRef, useState } from 'react';
import { isFirefox, isSafari } from 'utils/mobile';
import { getBlobDuration, getRecordedDate, getVideoScreen } from 'utils/video';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';

const videoOptions = {
    controls: false,
    autoplay: 'any',
    loop: true
};

interface Props {
    recordedData: Blob;
    onNavigationBack: () => void;
}

// NOTE: special case for loading player
const inlinePlayer = isFirefox || isSafari;

export const VideoViewer: FC<Props> = ({ recordedData, onNavigationBack }) => {
    const videoRef = useRef<HTMLVideoElement>(null);
    const [ready, setReady] = useState(false);
    const [player, setPlayer] = useState<ReturnType<typeof videojs>>();
    const [pause, togglePause] = useToggle();
    const [src, setSrc] = useState('');
    const [startVideo, setStartVideo] = useState(0);
    const [endVideo, setEndVideo] = useState(0);
    const [stopVideoInterval, setStopVideoInterval] = useState(0);

    useEffect(() => {
        const player = videojs(videoRef.current, videoOptions, () => {
            setReady(true);

            const src = URL.createObjectURL(recordedData);

            if (inlinePlayer) {
                player.src({
                    type: recordedData.type,
                    src
                });
            }

            setSrc(src);
        });

        setPlayer(player);

        return () => player.dispose();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [recordedData]);

    // handle restart video if timing changed
    useEffect(() => {
        clearInterval(stopVideoInterval);

        if (!endVideo || pause) return;

        const videoDurationSec = endVideo - startVideo;

        // reset video after duration is end
        const stopInterval = setInterval(() => {
            player?.currentTime(startVideo);
        }, videoDurationSec * 1000);

        setStopVideoInterval(stopInterval);

        return () => {
            clearInterval(stopVideoInterval);
            clearInterval(stopInterval);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [startVideo, endVideo, pause]);

    // pause video and show play button
    const handleSlideStart = () => {
        player?.pause();

        if (pause) return;

        togglePause();
    };

    const handleChangeTimeLine = (values: readonly number[]) => {
        const [start, end] = values;

        if (!player || values.length !== 2 || isNaN(start) || isNaN(end)) return;

        player.pause();

        // show changed frame
        if (startVideo !== start) {
            player.currentTime(start);
        } else {
            player.currentTime(end);
        }

        setStartVideo(start);
        setEndVideo(end);
    };

    // play video again
    const handlePlay = () => {
        if (!player) return;

        player.currentTime(startVideo);

        player.play();

        togglePause();
    };

    const handleNextStep = async () => {
        if (!recordedData) return;

        stepEffects.setStep(Steps.Login);

        const screen = await getVideoScreen(recordedData, startVideo);

        let videoDuration = endVideo - startVideo;

        if (!endVideo) {
            let { duration } = await getBlobDuration(recordedData);

            videoDuration = duration - startVideo;
        }

        videoEffects.setRecorderVideoScreen(screen);
        videoEffects.setRecorderVideoDuration(videoDuration);
        videoEffects.setRecorderVideoDate(getRecordedDate());
    };

    // TODO: фреймы на сафари не все отображаются
    return (
        <VideoWrapper>
            <div data-vjs-player="">
                <video ref={videoRef} playsInline className="video-js" src={inlinePlayer ? undefined : src} />
            </div>

            <NavigationBack onClick={onNavigationBack} />

            {pause && <PlayButton onClick={handlePlay} />}

            {ready && videoRef.current && (
                <Frames video={recordedData} onChange={handleChangeTimeLine} onSlideStart={handleSlideStart} />
            )}

            <Navigation circle onClick={handleNextStep} />
        </VideoWrapper>
    );
};
