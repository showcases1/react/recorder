import styled from 'styled-components/macro';

import PlayVideoIcon from 'assets/icons/video-play-button.svg';
import { NavigationButton } from 'components/common/NavigationButton';

export const PlayButton = styled.div`
    position: absolute;

    top: 50%;
    left: 50%;

    width: 64px;
    height: 64px;

    background-image: url(${PlayVideoIcon});

    transform: translate(-50%, -50%);

    cursor: pointer;
`;

export const NavigationBack = styled(NavigationButton).attrs({
    type: 'button'
})`
    position: absolute;

    top: 20px;
    left: 20px;
`;

export const Navigation = styled(NavigationButton)`
    position: absolute;

    bottom: 20px;
    right: 20px;
`;
