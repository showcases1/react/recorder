import styled from 'styled-components';

export const Wrapper = styled.div`
    position: relative;

    width: 100%;
    height: 100%;

    max-width: 520px;

    margin: auto;
`;
