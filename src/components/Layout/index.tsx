import React, { FC, useEffect } from 'react';
import { Wrapper } from './styles';

export const Layout: FC = ({ children }) => {
    useEffect(() => {}, []);

    return <Wrapper>{children}</Wrapper>;
};
