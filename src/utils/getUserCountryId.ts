import moment from 'moment-timezone';
import ct from 'countries-and-timezones';

export const getUserCountryId = (): string | null => {
    const countryObject = ct.getCountryForTimezone(moment.tz.guess());

    return countryObject && countryObject.id.toLowerCase();
};
