import { createEffect, Effect } from 'effector';
import { effects } from 'effects/notifications';


/**
 * Generate unique identificator
 */
export const uuid = () => {
    let dt = new Date().getTime();

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        const r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);

        return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });
};

/**
 * Create effect with bad response notifications
 */
interface BadResponseErrors
    extends Recorder.Error409ConflictResponse,
        Recorder.Error400BadRequest,
        Recorder.Error404NotFoundResponse {}

export function createNotifyingEffect<Params, Done>(config: {
    name?: string;
    handler?: (params: Params) => Promise<Done> | Done;
    sid?: string;
}): Effect<Params, Done> {
    const effect = createEffect(config);

    effect.fail.watch(({ error }: { error: BadResponseErrors }) => {
        if (!error.isSuccess) {
            let message = error.message;

            if (!message || message === 'unknown') {
                message = 'Some error occurred. Please try again.';
            }

            effects.setNotification({
                message,
                description: ''
            });
        }
    });

    return effect;
}
