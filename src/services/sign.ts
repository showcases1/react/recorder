import axios from 'services/axios';

export const checkUsernameExistence = (data: Recorder.UsernameExistsRequest) =>
    axios<Recorder.ExistenceResponse>({
        url: '/user/username-exists',
        data
    });

export const analyzeMobileNumber = (data: Recorder.AnalyzeMobileNumberRequest) =>
    axios<Recorder.AnalyzeMobileNumberResponse>({
        url: '/user/sms-analyze-mobile-number',
        data
    });

//TODO: [any]
export const analyzeEmail = (data: { email: string }) =>
    axios<any>({
        url: '/user/analyze-email',
        data
    });

export const sendVerificationSms = async (data: Recorder.SendVerificationSmsRequest) =>
    axios<Recorder.SendVerificationSmsResponse>({
        url: '/user/sms-send-verification',
        data
    });

export const checkSmsCode = (data: Recorder.CheckSmsCodeRequest) =>
    axios<Recorder.CheckSmsCodeResponse>({
        url: '/user/sms-check-verification-code',
        data
    });

export const createAccount = (data: Recorder.UserCreateAccountRequest) =>
    axios<Recorder.UserJwtTokenResponse>({
        url: '/user/create-account',
        data
    });

export const authenticateUser = (data: Recorder.UserAuthChallengeEmailOrUsernameOrPhoneRequest) =>
    axios<Recorder.UserJwtTokenResponse>({
        url: '/user/authenticate',
        data
    });

export const sendForgottenPasswordEmail = (data: Recorder.UserWantsForgottenPasswordRequest) =>
    axios<Recorder.MessageResponseBase>({
        url: '/user/send-forgotten-password-email',
        data
    });

export const setNewPassword = (data: Recorder.AuthenticateWithTokenRequest) =>
    axios<Recorder.MessageResponseBase>({
        url: '/user/authenticate-with-token',
        data
    });

export const getCurrentAuthorization = (data: Recorder.GetCurrentAuthorizationsRequest) =>
    axios<Recorder.GetCurrentAuthorizationsResponse>({
        url: '/user/get-current-authorization',
        data
    });
