import { isSafari } from 'utils/mobile';
import { getRecordedDate } from 'utils/video';
import axios from './axios';

export const upload = (videoId: string, videoStream: Blob) => {
    const bodyFormData = new FormData();

    // TODO: change name to recorded video time
    const fileName = `${getRecordedDate()}.${isSafari ? 'mp4' : 'webm'}`;
    bodyFormData.append('fileName', new File([videoStream], fileName));

    return axios<Recorder.MessageResponseBase>({
        url: `/media/upload/${videoId}`,
        data: bodyFormData,
        headers: { 'Content-Type': 'multipart/form-data' }
    });
};
