import axios from './axios';

export const createWOMWallet = () =>
    axios<Recorder.MessageResponseBase>({
        url: '/wom/create-wom-wallet'
    });

export const beginValidation = (data: Recorder.WOMStartValidationRequest) =>
    axios<Recorder.ValidationStateResponse>({
        url: '/wom/begin-validation',
        data
    });
