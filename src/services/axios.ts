import axios, { AxiosPromise, AxiosRequestConfig } from 'axios';
import { state } from 'effects/user';

const RecorderAxiosInstance = axios.create();

RecorderAxiosInstance.defaults.baseURL = 'https://Recorder.com/';
RecorderAxiosInstance.defaults.method = 'POST';
RecorderAxiosInstance.interceptors.response.use(
    config => config.data,
    config => Promise.reject(config.response.data)
);

export default <T = void>(config: AxiosRequestConfig, withToken = true) => {
    const request: AxiosPromise<T> = RecorderAxiosInstance({
        ...config,
        headers: withToken
            ? {
                  Authorization: `Bearer ${state.token.getState() || process.env.REACT_APP_AUTH_TOKEN}`,
                  ...config.headers
              }
            : { ...config.headers }
    });

    return (request as any) as Promise<T>;
};
