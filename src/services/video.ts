import axios from './axios';

export const newRequest = (data: Recorder.CreateNewVideoRequest) =>
    axios<Recorder.CreateNewVideoResponse>({
        url: '/video/new',
        data
    });
