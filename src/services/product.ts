import axios from 'services/axios';

export const queryListOfProducts = (data: Recorder.GetProductsByNameRequest) =>
    axios<Recorder.GetProductsResponse>({
        url: '/product/query-by-name',
        data
    });

export const createProduct = (data: Recorder.CreateProductRequest) =>
    axios<Recorder.CreateProductResponse>({
        url: '/product/create',
        data
    });
