import axios from './axios';

export const query = (data: Recorder.PlaylistVideosRequest) =>
    axios<Recorder.PlaylistVideosResponse>({
        url: '/playlist/query',
        data
    });
