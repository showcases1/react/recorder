import axios from './axios';

export const createAccountAnonymous = (data: Recorder.UserCreateAnonymousAccountRequest) =>
    axios<Recorder.UserJwtTokenResponse>(
        {
            url: '/user/create-account-anonymous',
            data
        },
        false
    );
