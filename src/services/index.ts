import * as user from './user';
import * as playlist from './playlist';
import * as sign from 'services/sign';
import * as product from 'services/product';
import * as video from 'services/video';
import * as media from 'services/media';
import * as wom from 'services/wom';

export const API = {
    user,
    playlist,
    sign,
    product,
    video,
    media,
    wom
};
