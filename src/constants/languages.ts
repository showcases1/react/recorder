export const languagesArray = [
    'Nothing is spoken',
    'Mandarin',
    'Spanish',
    'English',
    'Arabic',
    'Portuguese',
    'Russian',
    'Japanese',
    'German',
    'French',
    'Turkish',
    'Vietnamese',
    'Korean',
    'Italian'
];

export const languagesCodes: { [name: string]: string } = {
    ar: 'Arabic',
    en: 'English',
    es: 'Spanish',
    de: 'German',
    fr: 'French',
    ja: 'Japanese',
    ko: 'Korean',
    pt: 'Portuguese',
    ru: 'Russian',
    tr: 'Turkish',
    vi: 'Vietnamese',
    it: 'Italian'
};
