import { createNotifyingEffect } from 'utils/common';
import { API } from 'services';

export const queryListOfProducts = createNotifyingEffect({
    handler: async (name: string) =>
        await API.product.queryListOfProducts({
            name,
            limit: 5,
            returnQueryCount: true
        })
});

export const createProduct = createNotifyingEffect({
    handler: async (name: string) =>
        await API.product.createProduct({
            name,
            upc: ''
        })
});

const effects = {
    queryListOfProducts,
    createProduct
};

export { effects };
