import { createEffect, createEvent, createStore } from 'effector';
import { API } from 'services';

/**
 * Effect to load JWT token for anonymous user
 */
const loadToken = createEffect({
    handler: async () => {
        const { token } = await API.user.createAccountAnonymous({ locale: 'en_EN' });

        return token || '';
    }
});

const setUser = createEvent<Recorder.GetUserResponse>();
const user = createStore<Recorder.GetUserResponse>({ userId: '' }).on(setUser, (_, user: Recorder.GetUserResponse) => user);

const setToken = createEvent();
const token = createStore('')
    .on(loadToken.doneData, (_, token) => token)
    .on(setToken, (_, token) => token);

const effects = { loadToken, setUser, setToken };
const state = { user, token };

export { effects, state };
