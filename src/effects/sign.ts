import { createEffect, createEvent, createStore } from 'effector';
import { API } from 'services';
import { createNotifyingEffect } from 'utils/common';


//
/*** Username ***/
//
const checkUsernameExistence = createNotifyingEffect({
    handler: async (username: string) => {
        const { exists } = await API.sign.checkUsernameExistence({ username });

        return exists;
    }
});

//
/*** Chosen Registration Input ***/
//
const changeChosenSignInput = createEvent<'phone' | 'email' | ''>();
const chosenSignInput = createStore('').on(changeChosenSignInput, (_, chosenInput) => chosenInput);

//
/*** Mobile Number ***/
//
const analyzeMobileNumber = createNotifyingEffect({
    handler: async (mobileNumber: string) => {
        const { result } = await API.sign.analyzeMobileNumber({ mobileNumber });

        return result;
    }
});

//
/*** Email ***/
//
const analyzeEmail = createEffect({
    handler: async (email: string) => {
        const { result } = await API.sign.analyzeEmail({ email }).catch(err => err);

        return result;
    }
});

//
/*** Sms Code ***/
//
const sendSmsCode = createNotifyingEffect({
    handler: async (mobileNumber: string) => {
        await API.sign.sendVerificationSms({ mobileNumber });
    }
});
const checkSmsCode = createEffect({
    handler: async (data: Recorder.CheckSmsCodeRequest) => {
        const { result } = await API.sign.checkSmsCode(data);

        return result;
    }
});

//
/*** Forgotten Password ***/
//
const sendForgottenPasswordEmail = createEffect({
    handler: async (email: string) => await API.sign.sendForgottenPasswordEmail({ email }).catch(err => err)
});
const setNewPassword = createEffect({
    handler: async (data: Recorder.AuthenticateWithTokenRequest) => await API.sign.setNewPassword(data).catch(err => err)
});

//
/*** Auth ***/
//
const authenticateUser = createEffect({
    handler: async (data: Recorder.UserAuthChallengeEmailOrUsernameOrPhoneRequest) =>
        await API.sign.authenticateUser(data).catch(err => err)
});

//
/*** Creating Account ***/
//
const createAccount = createNotifyingEffect({
    handler: async (data: Recorder.UserCreateAccountRequest) => await API.sign.createAccount(data)
});

//
/*** Creating WOM Wallet ***/
//
const createWOMWallet = createNotifyingEffect({
    handler: async () => await API.wom.createWOMWallet()
});

//
/*** Get Current Authorization ***/
//
const getCurrentAuthorization = createNotifyingEffect({
    handler: async (userId: string) => await API.sign.getCurrentAuthorization({ userId })
});

//
/*** Exports ***/
//
const effects = {
    checkUsernameExistence,
    changeChosenSignInput,
    analyzeMobileNumber,
    analyzeEmail,
    sendSmsCode,
    checkSmsCode,
    sendForgottenPasswordEmail,
    setNewPassword,
    authenticateUser,
    createAccount,
    createWOMWallet,
    getCurrentAuthorization
};
const state = { chosenSignInput };

export { effects, state };
