import { createEvent, createStore } from 'effector';
import { API } from 'services';
import { createNotifyingEffect } from 'utils/common';


//
/*** Local store ***/
//
const setRecorderVideo = createEvent<Blob | null>();
const setRecorderVideoScreen = createEvent<string>();
const setRecorderVideoDuration = createEvent<number>();
const setRecorderVideoDate = createEvent<string>();

const recorderVideo = createStore<Blob | null>(null).on(setRecorderVideo, (_, video) => video);
const recorderVideoScreen = createStore<string>('').on(setRecorderVideoScreen, (_, screen) => screen);
const recorderVideoDuration = createStore<number>(0).on(setRecorderVideoDuration, (_, duration) => duration);
const recorderVideoDate = createStore<string>('').on(setRecorderVideoDate, (_, date) => date);

//
/*** API effects ***/
//
const createNewVideoRequest = createNotifyingEffect({
    handler: async (data: Recorder.CreateNewVideoRequest) => await API.video.newRequest(data)
});

const uploadVideo = createNotifyingEffect({
    handler: async (data: { videoId: string; videoStream: Blob }) =>
        await API.media.upload(data.videoId, data.videoStream)
});

const validateContent = createNotifyingEffect({
    handler: async (data: Recorder.WOMStartValidationRequest) => await API.wom.beginValidation(data)
});

//
/*** Return ***/
//
const effects = {
    setRecorderVideo,
    setRecorderVideoScreen,
    setRecorderVideoDuration,
    setRecorderVideoDate,
    createNewVideoRequest,
    uploadVideo,
    validateContent
};

const state = {
    recorderVideo,
    recorderVideoScreen,
    recorderVideoDuration,
    recorderVideoDate
};

export { state, effects };
