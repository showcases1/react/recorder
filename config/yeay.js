(function () {
    var prod = 'https://Recorder.com/static';

    function loadPlugin(version) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = prod + '/js/bundle.min.js?' + version; // TODO: get path from .env

        document.body.appendChild(script);
    }

    function getRandomVersion() {
        return Math.floor(Math.random() * (1000 - 1)) + 1;
    }

    if (typeof fetch === 'function') {
        fetch(prod + '/version.txt', { mode: 'no-cors' })
            .then(response =>
                response.text().then(version => loadPlugin(version ? version : getRandomVersion().toString()))
            )
            .catch(() => loadPlugin(getRandomVersion().toString()));
    } else {
        loadPlugin(getRandomVersion().toString());
    }
})();
