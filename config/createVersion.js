// read .env file
const dotenv = require('dotenv').config({ path: '.env' });
fs = require('fs');

// write version to build
fs.writeFileSync('build/static/version.txt', dotenv.parsed.VERSION);

// copy main bundle file
fs.copyFileSync('config/Recorder.js', 'build/static/js/Recorder.js');
