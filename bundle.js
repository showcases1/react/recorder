/**
 * Webpack config to create one JS bundle file
 */
const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const glob = require('glob');
const dotenv = require('dotenv').config({ path: __dirname + '/.env' });

module.exports = {
    mode: 'production',
    entry: {
        'bundle.js': glob.sync('build/static/?(js|css)/main.*.?(js|css)').map(f => path.resolve(__dirname, f))
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'static/js/bundle.min.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.woff$|\.woff2?$|\.ttf$|\.eot$|\.otf$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'static/media/',
                            publicPath: url => `${process.env.PRODUCTION_URL}/static/media/${url}`
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new UglifyJsPlugin(),
        new webpack.DefinePlugin({
            'process.env': dotenv.parsed
        })
    ]
};
