/**
 * Override webpack config to work with videojs-record
 * https://collab-project.github.io/videojs-record/#/frameworks/react
 */
const webpack = require('webpack');

module.exports = (config, env) => {
    // Extend the config to work with videojs-record without ejecting create react app.
    // Reference: https://collab-project.github.io/videojs-record/#/react
    const videojsPlugin = new webpack.ProvidePlugin({
        videojs: 'video.js/dist/video.cjs.js',
        RecordRTC: 'recordrtc'
    });
    const videojsAlias = {
        videojs: 'video.js',
        WaveSurfer: 'wavesurfer.js',
        RecordRTC: 'recordrtc'
    };

    config.resolve.alias = { ...config.resolve.alias, ...videojsAlias };
    config.plugins.push(videojsPlugin);

    // create build without chunks
    config.optimization.splitChunks = { chunks: 'async' };
    config.optimization.runtimeChunk = false;

    // server path for assets and chunks (path to project files)
    config.output.publicPath = process.env.NODE_ENV === 'production' ? process.env.PRODUCTION_URL : '/';

    return config;
};
